using System;

namespace IMDBAPI.Model.DB
{
    public class Review
    {
        public int Id { get; set; }
        public string ReviewText { get; set; }
    }
}
