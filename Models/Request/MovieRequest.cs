using System.Collections.Generic;

namespace IMDBAPI.Model.Request
{
    public class MovieRequest
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public string Plot { get; set; }
        public List<int> Actors { get; set; }
        public List<int> Genres { get; set; }
        public int ProducerId { get; set; }
        public string Poster { get; set; }
    }
}