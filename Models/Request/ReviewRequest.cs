using System;

namespace IMDBAPI.Model.DB
{
    public class ReviewRequest
    {
        public string Review { get; set; }
    }
}
