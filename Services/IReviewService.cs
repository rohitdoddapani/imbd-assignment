using System.Collections.Generic;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Service
{
    public interface IReviewService
    {
        public IEnumerable<ReviewResponse> Get(int movieId);
        ReviewResponse Get(int movieId,int reviewId);
        public void Post(int movieId,ReviewRequest review);
        public void Put(int movieId,int reviewId,ReviewRequest movie);
        public void Delete(int movieId,int reviewId);
    }
}