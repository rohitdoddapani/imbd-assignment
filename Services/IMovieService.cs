using System.Collections.Generic;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Service
{
    public interface IMovieService
    {
        public IEnumerable<MovieResponse> Get();
        MovieResponse Get(int id);
        public void Post(MovieRequest movie);
        public void Put(int id,MovieRequest movie);
        public void Delete(int id);
        //public IEnumerable<ReviewResponse> GetReviews(int id);
    }
}