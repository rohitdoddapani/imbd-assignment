using System.Collections.Generic;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Service
{
    public interface IActorService
    {
        public IEnumerable<ActorResponse> Get();
        
        ActorResponse Get(int id);

        public void Post(ActorRequest actor);

        public void Put(int id,ActorRequest actor);
        
        public void Delete(int id);
    }
}