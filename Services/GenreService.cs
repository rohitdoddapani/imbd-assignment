using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;
using IMDBAPI.Service;

namespace IMDBAPI.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;

        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
        }

        void IGenreService.Delete(int id)
        {
            var genres = _genreRepository.Get(id);
            if (genres != null)
            {
                _genreRepository.Delete(id);
            }
            else
            {
                throw new Exception();
            }

        }

        IEnumerable<GenreResponse> IGenreService.Get()
        {
            var genres = _genreRepository.Get();
            return genres.Select(a => new GenreResponse
            {
                Id = a.Id,
                Name = a.Name
            });
        }

        GenreResponse IGenreService.Get(int id)
        {
            try
            {
                var genres = _genreRepository.Get(id);
                var res = new GenreResponse
                {
                    Id = genres.Id,
                    Name = genres.Name
                };
                return res;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        void IGenreService.Post(GenreRequest genre)
        {
            try
            {
                _genreRepository.Post(genre);
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        void IGenreService.Put(int id, GenreRequest genre)
        {

            var genres = _genreRepository.Get(id);
            if (genres != null)
            {
                _genreRepository.Put(id, genre);
            }
            else
            {
                throw new Exception();
            }
        }
    }
}