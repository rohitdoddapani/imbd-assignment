using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;
using IMDBAPI.Service;

namespace IMDBAPI.Services
{
    public class ReviewService : IReviewService
    {

        private readonly IReviewRepository _reviewRepository;

        public ReviewService(IReviewRepository reviewRepository)
        {
            _reviewRepository = reviewRepository;
        }
        
        public IEnumerable<ReviewResponse> Get(int movieId)
        {
            try
            {
                var reviews = _reviewRepository.Get(movieId);

                var reviewsList = new List<ReviewResponse>();

                reviews.ToList().ForEach( review =>
                {
                    var tempMovie = new ReviewResponse
                    {
                        Id = review.Id,
                        Review = review.ReviewText
                    };
                    reviewsList.Add(tempMovie);
                });
                return reviewsList;
            }
            catch (Exception)
            {
                
                throw;
            }
            
            
        }

        public ReviewResponse Get(int movieId,int reviewId)
        {
            try
            {
                var reviews = _reviewRepository.Get(movieId, reviewId);
                var res = new ReviewResponse
                {
                    Id = reviews.Id,
                    Review = reviews.ReviewText
                };
                return res;
            }
            catch (Exception)
            {
                throw;
            }
            
        }

        public void Post(int movieId,ReviewRequest review)
        {
            try
            {
                _reviewRepository.Post(movieId,review);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Put(int movieId,int reviewId,ReviewRequest review)
        {
            try
            {
                Get(movieId, reviewId);
                _reviewRepository.Put(movieId, reviewId, review);
            }
            catch (Exception)
            {
                throw new Exception();
            }
            
        }

        public void Delete(int movieId,int reviewId){
            _reviewRepository.Delete(movieId,reviewId);
        }

    }
}
