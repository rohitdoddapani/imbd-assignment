using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;
using IMDBAPI.Service;

namespace IMDBAPI.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;

        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }
        
        void IActorService.Delete(int id)
        {
            var actors = _actorRepository.Get(id);
            if (actors != null)
            {
                _actorRepository.Delete(id);
            }
            else
            {
                throw new Exception();
            }

        }

        IEnumerable<ActorResponse> IActorService.Get()
        {
            var actors = _actorRepository.Get();
            return actors.Select(a => new ActorResponse
            {
                Id = a.Id,
                Name = a.Name,
                DOB = a.DOB,
                Bio = a.Bio,
                Gender = a.Gender
            });
        }

        ActorResponse IActorService.Get(int id)
        {
            var actors = _actorRepository.Get(id);
            if (actors == null)
            {
                throw new Exception();
            }
            var res = new ActorResponse
            {
                Id = actors.Id,
                Name = actors.Name,
                DOB = actors.DOB,
                Bio = actors.Bio,
                Gender = actors.Gender
            };
            return res;
        }

        void IActorService.Post(ActorRequest actor)
        {
            _actorRepository.Post(actor);
        }

        void IActorService.Put(int id, ActorRequest actor)
        {
            var actors = _actorRepository.Get(id);
            if (actors != null)
            {
                _actorRepository.Put(id, actor);
            }
            else
            {
                throw new Exception();
            }
        }
    }
}