using System.Collections.Generic;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Service
{
    public interface IProducerService
    {
        public IEnumerable<ProducerResponse> Get();
        
        ProducerResponse Get(int id);

        public void Post(ProducerRequest producer);

        public void Put(int id,ProducerRequest producer);
        public void Delete(int id);
    }
}