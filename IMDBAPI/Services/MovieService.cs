using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;
using IMDBAPI.Service;

namespace IMDBAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly IActorRepository _actorRepository;
        private readonly IActorService _actorService;
        private readonly IProducerRepository _producerRepository;
        private readonly IProducerService _producerService;
        private readonly IMovieRepository _movieRepository;
        private readonly IGenreRepository _genreRepository;
        private readonly IGenreService _genreService;

        public MovieService(IMovieRepository movieRepository, IActorRepository actorRepository,
            IProducerRepository producerRepository, IGenreRepository genreRepository,
            IActorService actorService, IGenreService genreService, IProducerService producerService)
        {
            _actorRepository = actorRepository;
            _producerRepository = producerRepository;
            _movieRepository = movieRepository;
            _genreRepository = genreRepository;
            _actorService = actorService;
            _producerService = producerService;
            _genreService = genreService;
        }

        public IEnumerable<MovieResponse> Get()
        {
            try
            {
                var movies = _movieRepository.Get();

                var moviesList = new List<MovieResponse>();

                movies.ToList().ForEach(movie =>
                {
                    var actor = _actorRepository.GetActorsByMovieId(movie.Id);
                    var genre = _genreRepository.GetGenresByMovieId(movie.Id);
                    var producer = _producerRepository.Get(movie.ProducerId);

                    var tempMovie = new MovieResponse
                    {
                        Id = movie.Id,
                        Name = movie.Name,
                        Year = movie.Year,
                        Plot = movie.Plot,
                        Poster = movie.Poster,
                        Actors = actor.ToList(),
                        Genres = genre.ToList(),
                        Producer = producer
                    };
                    moviesList.Add(tempMovie);
                });
                return moviesList;
            }
            catch (Exception)
            {

                throw;
            }


        }

        public MovieResponse Get(int id)
        {
            var movie = _movieRepository.Get().SingleOrDefault(m => m.Id == id);
            var res = new MovieResponse
            {
                Id = movie.Id,
                Name = movie.Name,
                Year = movie.Year,
                Actors = _actorRepository.GetActorsByMovieId(movie.Id).ToList(),
                Genres = _genreRepository.GetGenresByMovieId(movie.Id).ToList(),
                Producer = _producerRepository.Get(movie.ProducerId),
                Plot = movie.Plot,
                Poster = movie.Poster
            };
            return res;
        }

        public void Post(MovieRequest movie)
        {
            _producerService.Get(movie.ProducerId);
            movie.Actors.ForEach(a => _actorService.Get(a));
            movie.Genres.ForEach(g => _genreService.Get(g));
            _movieRepository.Post(movie);
        }

        public void Put(int id, MovieRequest movie)
        {
            Get(id);
            _producerService.Get(movie.ProducerId);
            movie.Actors.ForEach(a => _actorService.Get(a));
            movie.Genres.ForEach(g => _genreService.Get(g));
            _movieRepository.Put(id, movie);
        }

        public void Delete(int id)
        {
            try
            {
                var movie = _movieRepository.Get().SingleOrDefault(m => m.Id == id);
                if (movie == null)
                {
                    throw new Exception();
                }
                _movieRepository.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
    }
}
