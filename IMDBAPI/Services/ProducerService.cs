using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;
using IMDBAPI.Service;

namespace IMDBAPI.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;

        public ProducerService(IProducerRepository producerRepository)
        {
            _producerRepository = producerRepository;
        }

        void IProducerService.Delete(int id)
        {
            var producers = _producerRepository.Get(id);
            if (producers != null)
            {
                _producerRepository.Delete(id);
            }
            else
            {
                throw new Exception();
            }

        }

        IEnumerable<ProducerResponse> IProducerService.Get()
        {
            var producers = _producerRepository.Get();
            return producers.Select(p => new ProducerResponse
            {
                Id = p.Id,
                Name = p.Name,
                DOB = p.DOB,
                Bio = p.Bio,
                Gender = p.Gender
            });
        }

        ProducerResponse IProducerService.Get(int id)
        {
            try
            {
                var producer = _producerRepository.Get(id);
                var res = new ProducerResponse
                {
                    Id = producer.Id,
                    Name = producer.Name,
                    DOB = producer.DOB,
                    Bio = producer.Bio,
                    Gender = producer.Gender
                };
                return res;

            }
            catch (Exception)
            {
                throw;
            }
        }

        void IProducerService.Post(ProducerRequest producer)
        {
            try
            {
                _producerRepository.Post(producer);
            }
            catch (Exception)
            {
                throw;
            }
        }

        void IProducerService.Put(int id, ProducerRequest producer)
        {
            var validProducer = _producerRepository.Get(id);
            if (validProducer != null)
            {
                _producerRepository.Put(id, producer);
            }
            else
            {
                throw new Exception();
            }
        }
    }
}