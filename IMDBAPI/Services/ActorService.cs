using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;
using IMDBAPI.Repository;
using IMDBAPI.Service;

namespace IMDBAPI.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;

        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }
        
        void IActorService.Delete(int id)
        {
            var actor = _actorRepository.Get(id);
            if (actor != null)
            {
                _actorRepository.Delete(id);
            }
            else
            {
                throw new Exception();
            }

        }

        IEnumerable<ActorResponse> IActorService.Get()
        {
            var actors = _actorRepository.Get();
            return actors.Select(a => new ActorResponse
            {
                Id = a.Id,
                Name = a.Name,
                DOB = a.DOB,
                Bio = a.Bio,
                Gender = a.Gender
            });
        }

        IEnumerable<ActorResponse> IActorService.GetByMovieId(int id)
        {
            var actors = _actorRepository.GetActorsByMovieId(id);
            if (!actors.Any())
            {
                throw new Exception();
            }
            return actors.Select(a => new ActorResponse
            {
                Id = a.Id,
                Name = a.Name,
                DOB = a.DOB,
                Bio = a.Bio,
                Gender = a.Gender
            });
        }

        ActorResponse IActorService.Get(int id)
        {
            var actor = _actorRepository.Get(id);
            if (actor == null)
            {
                throw new Exception();
            }
            var res = new ActorResponse
            {
                Id = actor.Id,
                Name = actor.Name,
                DOB = actor.DOB,
                Bio = actor.Bio,
                Gender = actor.Gender
            };
            return res;
        }

        void IActorService.Post(ActorRequest actor)
        {
            _actorRepository.Post(actor);
        }

        void IActorService.Put(int id, ActorRequest actor)
        {
            var validActor = _actorRepository.Get(id);
            if (validActor != null)
            {
                _actorRepository.Put(id, actor);
            }
            else
            {
                throw new Exception();
            }
        }
    }
}