using System.Collections.Generic;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;

namespace IMDBAPI.Service
{
    public interface IGenreService
    {
        public IEnumerable<GenreResponse> Get();
        
        GenreResponse Get(int id);

        public void Post(GenreRequest genre);

        public void Put(int id,GenreRequest genre);
        public void Delete(int id);
    }
}