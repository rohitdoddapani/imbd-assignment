using System;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IMDBAPI.Controllers
{
    [ApiController]
    [Route("api/movies/{movieId:int}/reviews")]
    public class ReviewController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [HttpGet]
        public IActionResult Get(int movieId)
        {
            try
            {
                var reviews = _reviewService.Get(movieId);
                return Ok(reviews);
            }
            catch (Exception)
            {
                return NotFound("Reviews not found");
            }
            
        }

        [HttpGet("{reviewId:int}")]
        public IActionResult Get(int movieId,int reviewId)
        {
            try
            {
                var review = _reviewService.Get(movieId, reviewId);
                return Ok(review);
            }
            catch (Exception)
            {
                return NotFound("review not found");
            }
            
        }

        [HttpPost]
        public IActionResult Post(int movieId,[FromBody]ReviewRequest review)
        {
            try
            {
                _reviewService.Post(movieId, review);
                return StatusCode(StatusCodes.Status201Created);
            }
            catch (Exception)
            {
                return BadRequest("Review create failed");
            }
            
        }

        [HttpPut("{reviewId:int}")]
        public IActionResult Put(int movieId,int reviewId,[FromBody]ReviewRequest review){
            try
            {
                _reviewService.Put(movieId, reviewId, review);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                return BadRequest("review update failed");
            }
            
        }

        [HttpDelete("{reviewId:int}")]
        public IActionResult Delete(int movieId,int reviewId){
            try
            {
                _reviewService.Delete(movieId, reviewId);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (Exception )
            {
                return BadRequest("review can't be deleted");
            }
            
        }
    }
}
