using System;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IMDBAPI.Repository
{
    [ApiController]
    [Route("api/genres")]
    public class GenreController : ControllerBase
    {
        private readonly IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_genreService.Get());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var genre = _genreService.Get(id);
                return Ok(genre);
            }
            catch (System.Exception)
            {
                return NotFound("Genre not found");
            }

        }

        [HttpPost]
        public IActionResult Post([FromBody] GenreRequest genre)
        {
            try
            {
                _genreService.Post(genre);
                return StatusCode(StatusCodes.Status201Created);
            }
            catch (Exception)
            {
                return BadRequest("Genre can't be created");

            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] GenreRequest genre)
        {
            try
            {
                _genreService.Put(id, genre);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                return BadRequest("Genre can't be updated");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _genreService.Delete(id);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                return BadRequest("Genre can't be deleted");
            }
        }
    }
}