﻿using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Firebase.Storage;
using System.Threading.Tasks;
using System;

namespace IMDBAPI.Controllers
{
    [ApiController]
    [Route("api/movies")]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_movieService.Get());
            }
            catch (System.Exception)
            {
                return NotFound("Error fetching movies");
            }
            
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var movie = _movieService.Get(id);
                return Ok(movie);
            }
            catch (System.Exception)
            {
               return NotFound("Movie not found");
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]MovieRequest movie)
        {
            try{
                _movieService.Post(movie);
                return StatusCode(StatusCodes.Status201Created);
            }
            catch(Exception){
                return BadRequest("movie creation failed");
            }
        }

        [HttpPut("{id:int}")]
        public IActionResult Put(int id,[FromBody]MovieRequest movie){
            try
            {
                _movieService.Put(id,movie);
                return StatusCode(StatusCodes.Status200OK);    
            }
            catch (Exception)
            {
                return BadRequest("movie update failed");
            }
            
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id){
            try
            {
                _movieService.Delete(id);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                return NotFound("movie delete failed");
            }
        }

        // [HttpGet("{id}/reviews")]
        // public IActionResult GetReviews(int id)
        // {
        //     var reviews = _movieService.GetReviews(id);
        //     if(reviews is null){
        //         return NotFound("Reviews not found");
        //     }
        //     return Ok(reviews);
        // }

        
        // firebase upload
        [HttpPost("upload")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
        if (file == null || file.Length == 0)
        return Content("file not selected");
        var task = await new FirebaseStorage("imdbapi-deltax.appspot.com")
                .Child("movieImages")
                .Child(Guid.NewGuid().ToString() + ".jpg")
                .PutAsync(file.OpenReadStream());
                return Ok(task);
        }

    }
}
