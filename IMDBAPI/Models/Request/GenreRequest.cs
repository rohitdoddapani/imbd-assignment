namespace IMDBAPI.Model.Request
{
    public class GenreRequest
    {
        public string Name { get; set; }
    }
}