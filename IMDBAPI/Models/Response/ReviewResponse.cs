using System;

namespace IMDBAPI.Model.DB
{
    public class ReviewResponse
    {
        public int Id { get; set; }
        public string Review { get; set; }
    }
}
