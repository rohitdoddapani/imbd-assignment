using System;

namespace IMDBAPI.Model.DB
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
