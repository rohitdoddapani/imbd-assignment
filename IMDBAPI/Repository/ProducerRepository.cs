using System;
using System.Collections.Generic;
using Dapper;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using System.Data.SqlClient;

namespace IMDBAPI.Repository
{
    public class ProducerRepository : IProducerRepository
    {
        private readonly IGenericRepository<Producer> _genericRepository;

        public ProducerRepository(IGenericRepository<Producer> genericRepository)
        {
            _genericRepository = genericRepository;
        }

        void IProducerRepository.Delete(int id)
        {
            const string query =
                    @"DELETE FROM Movies WHERE Movies.ProducerID=@ProducerId
                    DELETE FROM PRODUCERS WHERE ID=@ProducerId";
            
            _genericRepository.Delete(query, new
            {
                ProducerId = id
            });
        }

        IEnumerable<Producer> IProducerRepository.Get()
        {
            const string sql = @"
                        SELECT *
                        FROM Producers";

            var producers = _genericRepository.GetAll(sql);
            return producers;
        }

        Producer IProducerRepository.Get(int id)
        {
            const string sql = @"
                        SELECT Id [Id],
                        Name [Name],
                        Bio [Bio],
                        DOB [DOB],
                        Gender [Gender]
                        FROM Producers
                        WHERE Id = @ProducerId";

            var producer = _genericRepository.GetById(sql, new
            {
                ProducerId = id
            });
            return producer;
        }

        void IProducerRepository.Post(ProducerRequest producer)
        {
            const string query = 
                    @"
                    INSERT INTO Producers
                    (
                        [Name]
                        ,[Gender]
                        ,[DOB]
                        ,[Bio]
                    )
                    VALUES 
                    (
                        @Name
                        ,@Gender
                        ,@DOB
                        ,@Bio
                    )";
            
            _genericRepository.Post(query, new
            {
                Name = producer.Name,
                Gender = producer.Gender,
                DOB = producer.DOB,
                Bio = producer.Bio
            });
        }

        void IProducerRepository.Put(int id, ProducerRequest producer)
        {
            string query = 
                @"UPDATE Producers
                SET Name = @Name
                ,Gender = @Gender
                ,DOB = @DOB
                ,Bio = @Bio
                WHERE ID = @Id";
            
            _genericRepository.Put(query, new
            {
                Id = id,
                Name = producer.Name,
                Gender = producer.Gender,
                DOB = producer.DOB,
                Bio = producer.Bio
            });
        }
    }
}