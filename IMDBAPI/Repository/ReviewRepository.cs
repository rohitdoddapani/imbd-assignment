using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;
using System.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace IMDBAPI.Repository
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly Connection _connection;

        public ReviewRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }
        
        public IEnumerable<Review> Get(int movieId)
        {
            const string sql = @"
                        SELECT R.ID [Id],
                        R.Review [ReviewText]
                        FROM Reviews R
                        JOIN MovieReviewMapping MR
                        ON R.Id=MR.ReviewId
                        WHERE MR.MovieId=@MovieId";

            using var connection = new SqlConnection(_connection.DB);
            var reviews = connection.Query<Review>(sql, new
            {
                MovieId = movieId
            });
            return reviews;
        }

        Review IReviewRepository.Get(int movieId,int reviewId)
        {
            const string sql = @"
                        SELECT R.ID [Id],
                        R.Review [ReviewText]
                        FROM Reviews R
                        JOIN MovieReviewMapping MR
                        ON R.Id=MR.ReviewId
                        WHERE MR.MovieId=@MovieId and R.Id=@ReviewId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var reviews = connection.QuerySingleOrDefault<Review>(sql, new {
                    MovieId = movieId,
                    ReviewId = reviewId
                });
                return reviews;
            }
        }

        public void Post(int movieId,ReviewRequest review){
            using var connection = new SqlConnection(_connection.DB);
            const string query = @"
            INSERT INTO Reviews VALUES(@Review)

            INSERT INTO MovieReviewMapping VALUES(@MovieId,SCOPE_IDENTITY())
            ";
            var result = connection.Execute(query, new {
                    MovieId = movieId,
                    Review = review.Review
                });
        }

        public void Put(int movieId,int reviewId,ReviewRequest review){
            using var connection = new SqlConnection(_connection.DB);
            const string query = @"
            UPDATE Reviews SET Review=@Review WHERE ID=@ReviewId
            ";
            var result = connection.Execute(query, new {
                    MovieId = movieId,
                    ReviewId = reviewId,
                    Review = review.Review
                });
        }

        public void Delete(int movieId,int reviewId){
            const string query = @"
                DELETE FROM MovieReviewMapping WHERE ReviewId=@ReviewId

                DELETE FROM Reviews WHERE ID=@ReviewId";

            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query, new
            {
                MovieId = movieId,
                ReviewId = reviewId
            });
        }

        
    }
}