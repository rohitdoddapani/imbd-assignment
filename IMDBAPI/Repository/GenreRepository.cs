using System;
using System.Collections.Generic;
using Dapper;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using System.Data.SqlClient;

namespace IMDBAPI.Repository
{
    public class GenreRepository : IGenreRepository
    {
        private readonly IGenericRepository<Genre> _genericRepository;

        public GenreRepository(IGenericRepository<Genre> genericRepository)
        {
            _genericRepository = genericRepository;
        }

        void IGenreRepository.Delete(int id)
        {
            const string query = @"
                DELETE FROM MovieGenreMapping WHERE GenreId=@GenreId
                DELETE FROM Genres WHERE ID=@GenreId";

            _genericRepository.Delete(query, new
            {
                GenreId = id
            });
        }

        IEnumerable<Genre> IGenreRepository.Get()
        {
            const string sql = @"
                        SELECT *
                        FROM Genres";

            var genres = _genericRepository.GetAll(sql);
            return genres;
        }

        IEnumerable<Genre> IGenreRepository.GetGenresByMovieId(int id)
        {
            const string sql = @"
                        SELECT G.Id [Id],
                        G.Name [Name]
                        FROM Genres G
                        JOIN MovieGenreMapping MA
                        ON G.Id=MA.GenreId
                        WHERE MA.MovieId=@MovieId";

            /*using (var connection = new SqlConnection(_connection.DB))
            {
                var genres = connection.Query<Genre>(sql, new
                {
                    MovieId = id
                });
                return genres;
            }*/
            var genres = _genericRepository.GetGenresByMovieId(sql, new
            {
                MovieId = id
            });
            return genres;
        }
        Genre IGenreRepository.Get(int id)
        {
            const string sql = @"
                        SELECT *
                        FROM Genres
                        WHERE Id=@GenreId";

            var genre = _genericRepository.GetById(sql, new
            {
                GenreId = id
            });
            return genre;
        }

        void IGenreRepository.Post(GenreRequest genre)
        {
            const string query = 
                    @"
                    INSERT INTO Genres
                    (
                        [Name]
                    )
                    VALUES 
                    (
                        @Name
                    )";
            
            _genericRepository.Post(query, new
            {
                Name = genre.Name
            });
        }

        void IGenreRepository.Put(int id, GenreRequest genre)
        {
            string query = 
                @"UPDATE Genres
                SET Name = @Name
                WHERE Genres.ID = @Id";
            
            _genericRepository.Put(query, new
            {
                Id = id,
                Name = genre.Name,
            });
        }
    }
}