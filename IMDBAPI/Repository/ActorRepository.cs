using System;
using System.Collections.Generic;
using Dapper;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using System.Data.SqlClient;

namespace IMDBAPI.Repository
{
    public class ActorRepository : IActorRepository
    {
        private readonly IGenericRepository<Actor> _genericRepository;

        public ActorRepository(IGenericRepository<Actor> genericRepository)
        {
            _genericRepository = genericRepository;
        }
        void IActorRepository.Delete(int id)
        {
            const string query = @"
                DELETE FROM MovieActorMapping WHERE ActorId=@ActorId
                DELETE FROM ACTORS WHERE ID=@ActorId";

            _genericRepository.Delete(query, new
            {
                ActorId = id
            });
        }

        
        IEnumerable<Actor> IActorRepository.Get()
        {
            const string sql = @"
                        SELECT *
                        FROM Actors";

            var actors = _genericRepository.GetAll(sql);
            return actors;
        }

        IEnumerable<Actor> IActorRepository.GetActorsByMovieId(int id)
        {
            const string sql = @"
                        SELECT A.Id [Id]
                        ,A.Name [Name]
                        ,A.Bio [Bio]
                        ,A.DOB [DOB]
                        ,A.Gender [Gender]
                        FROM Actors A
                        JOIN MovieActorMapping MA
                        ON A.Id=MA.ActorId
                        WHERE MA.MovieId=@MovieId";

            var actors = _genericRepository.GetActorsByMovieId(sql, new
            {
                MovieId = id
            });
            return actors;

        }

        Actor IActorRepository.Get(int id)
        {
            const string sql = @"
                        SELECT *
                        FROM Actors
                        WHERE Id=@ActorId";

            var actor = _genericRepository.GetById(sql, new
            {
                ActorId = id
            });
            return actor;
            
        }

        void IActorRepository.Post(ActorRequest actor)
        {
            const string query = 
                    @"
                    INSERT INTO [dbo].[Actors]
                    (
                        [Name]
                        ,[Gender]
                        ,[DOB]
                        ,[Bio]
                    )
                    VALUES 
                    (
                        @Name
                        ,@Gender
                        ,@DOB
                        ,@Bio
                    )";

            _genericRepository.Post(query, new
            {
                Name = actor.Name,
                Gender = actor.Gender,
                DOB = actor.DOB,
                Bio = actor.Bio
            });
            
        }

        void IActorRepository.Put(int id, ActorRequest actor)
        {
            string query = 
                @"UPDATE Actors
                SET Name = @Name
                ,Gender = @Gender
                ,DOB = @DOB
                ,Bio = @Bio
                WHERE Actors.ID = @Id";

            _genericRepository.Put(query, new
            {
                Id=id,
                Name = actor.Name,
                Gender = actor.Gender,
                DOB = actor.DOB,
                Bio = actor.Bio
            });
            
            
        }
    }
}