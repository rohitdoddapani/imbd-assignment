using System.Collections.Generic;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;

namespace IMDBAPI.Repository
{
    public interface IGenreRepository
    {
        public IEnumerable<Genre> Get();
        public IEnumerable<Genre> GetGenresByMovieId(int id);
        public Genre Get(int id);

        public void Post(GenreRequest actor);
        public void Put(int id,GenreRequest actor);

        public void Delete(int id);
    }
}