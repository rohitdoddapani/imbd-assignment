﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;


namespace IMDBAPI.Repository
{
    public class GenericRepository<Tclass> : IGenericRepository<Tclass> 
    {
        private readonly Connection _connection;

        public GenericRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }


        public IEnumerable<Tclass> GetAll(string query)
        {
            using var connection = new SqlConnection(_connection.DB);
            return connection.Query<Tclass>(query);
        }

        public Tclass GetById(string query,object idObj)
        {
            using var connection = new SqlConnection(_connection.DB);
            return connection.QueryFirstOrDefault<Tclass>(query,idObj);
        }

        public void Post(string query,object obj)
        {
            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query, obj);
        }

        public void Put(string query, object obj)
        {
            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query, obj);
        }

        public void Delete(string query, object obj)
        {
            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query,obj);
        }

        public IEnumerable<Tclass> GetActorsByMovieId(string query, object obj)
        {
            using var connection = new SqlConnection(_connection.DB);
            var actors = connection.Query<Tclass>(query, obj);
            return actors;
        }

        public IEnumerable<Tclass> GetGenresByMovieId(string query, object obj)
        {
            using var connection = new SqlConnection(_connection.DB);
            var genres = connection.Query<Tclass>(query, obj);
            return genres;
        }
    }
}
