﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace IMDBAPI.Repository
{
    public interface IGenericRepository<Tclass> 
    {
        IEnumerable<Tclass> GetAll(string query);
        Tclass GetById(string query,object obj);
        void Post(string query,object obj);
        void Put(string query,object obj);
        void Delete(string query,object obj);

        IEnumerable<Tclass> GetActorsByMovieId(string query, object obj);
        IEnumerable<Tclass> GetGenresByMovieId(string query, object obj);

        
    }
}