using System.Collections.Generic;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;

namespace IMDBAPI.Repository
{
    public interface IProducerRepository
    {
        public IEnumerable<Producer> Get();
        
        public Producer Get(int id);

        public void Post(ProducerRequest actor);
        public void Put(int id,ProducerRequest actor);

        public void Delete(int id);
    }
}