
CREATE TABLE Actors(
	Id INT PRIMARY KEY,Name VARCHAR(100),Gender VARCHAR(10),DOB DATE);

CREATE TABLE Producers(
	Id INT PRIMARY KEY,Name VARCHAR(100),Company VARCHAR(10),Estd DATE);

CREATE TABLE Movies(
	Id INT PRIMARY KEY,
	Name VARCHAR(100),
	Language VARCHAR(10),
	ProducerId INT FOREIGN KEY REFERENCES Producers(Id),
	Profit INT
);

CREATE TABLE MovieActorMapping(
	MovieId INT FOREIGN KEY REFERENCES Movies(Id),
	ActorId INT FOREIGN KEY REFERENCES Actors(Id)
);

INSERT INTO Actors VALUES(1,'Mila Kunis','Female','11/14/1986');
INSERT INTO Actors VALUES(2,'Robert DeNiro','Male','07/10/1957')
INSERT INTO Actors VALUES(3,'George Michael','Male','11/23/1978')
INSERT INTO Actors VALUES(4,'Mike Scott','Male','08/06/1969')
INSERT INTO Actors VALUES(5,'Pam Halpert','Female','09/26/1996')
INSERT INTO Actors VALUES(6,'Dame Judi Dench','Female','04/05/1947')
INSERT INTO Actors VALUES(7,'Added','Male','07/10/1927')

INSERT INTO Producers VALUES(1,'Arjun','Fox','05/14/1998')
INSERT INTO Producers VALUES(2,'Arun','Bull','09/11/2004')
INSERT INTO Producers VALUES(3,'Tom','Hanks','11/03/1987')
INSERT INTO Producers VALUES(4,'Zeshan','Male','11/14/1996')
INSERT INTO Producers VALUES(5,'Nicole','Team Coco','09/26/1992')
INSERT INTO Producers VALUES(6,'AADITH','DELTAX','09/26/1992')

SELECT M.Name,COUNT(MA.ActorId)
FROM MovieActorMapping MA
INNER JOIN Movies M
ON MA.MovieId=M.Id
GROUP BY M.Id,M.Name


SELECT *
FROM PRODUCERS P
LEFT JOIN Movies M
ON P.Id=M.ProducerId
WHERE M.Id is NULL

INSERT INTO Movies VALUES(1,'Rocky','English',1,10000)
INSERT INTO Movies VALUES(2,'Rocky','Hindi',3,3000)
INSERT INTO Movies VALUES(3,'Terminal','English',4,300000)
INSERT INTO Movies VALUES(4,'Rambo','Hindi',2,93000)
INSERT INTO Movies VALUES(5,'Rudy','English',5,9600)

DELETE FROM Actors WHERE Id=7;

INSERT INTO MovieActorMapping VALUES(1,1)
INSERT INTO MovieActorMapping VALUES(1,3)
INSERT INTO MovieActorMapping VALUES(1,5)
INSERT INTO MovieActorMapping VALUES(2,6)
INSERT INTO MovieActorMapping VALUES(2,5)
INSERT INTO MovieActorMapping VALUES(2,4)
INSERT INTO MovieActorMapping VALUES(2,2)
INSERT INTO MovieActorMapping VALUES(3,3)
INSERT INTO MovieActorMapping VALUES(3,2)
INSERT INTO MovieActorMapping VALUES(4,1)
INSERT INTO MovieActorMapping VALUES(4,6)
INSERT INTO MovieActorMapping VALUES(4,3)
INSERT INTO MovieActorMapping VALUES(5,2)
INSERT INTO MovieActorMapping VALUES(5,5)
INSERT INTO MovieActorMapping VALUES(5,3)
INSERT INTO MovieActorMapping VALUES(3,7)

DELETE FROM MovieActorMapping WHERE ActorId=7;

--Update Profit of all the movies by +1000 where producer name contains 'run'
--SELECT Profit FROM Movies
UPDATE Movies
SET Profit+=1000
FROM Movies M
JOIN Producers P
ON M.ProducerId=P.Id
WHERE P.Name LIKE '%run%'
--SELECT Profit FROM Movies

--Find duplicate movies having the same name and their count
SELECT Name,COUNT(*) AS [Count]
FROM Movies
GROUP BY Name

--Find the oldest actor/actress for each movie

--not sure it is correct

SELECT TOP (SELECT COUNT(Name) FROM Movies) A.Name, M.Name,min(DOB) AS DOB
FROM Movies M
JOIN MovieActorMapping MA
ON M.Id=MA.MovieId
JOIN Actors A
ON A.Id=MA.ActorId
GROUP BY M.Name,A.Name
ORDER BY DOB


select xyz.Name,a1.Name,a1.DOB
FROM Actors a1
inner join (
SELECT m.Name,min(a.dob) as dob
FROM  Movies m
join MovieActorMapping ma
on m.Id=ma.MovieId
JOIN Actors a
ON ma.ActorId=a.Id
GROUP BY m.Name
)
as xyz
on a1.DOB=xyz.dob


--List of producers who have not worked with actor X
Select P.Name 
FROM Producers P
JOIN Movies M
ON P.Id=M.ProducerId
WHERE P.Id NOT IN (SELECT m.ProducerId
FROM Movies m
JOIN MovieActorMapping ma
ON ma.MovieId=m.Id
Join Actors a
ON a.Id=ma.ActorId
WHERE a.Name = 'Mila Kunis')


--List of pair of actors who have worked together in more than 2 movies

SELECT A1.Name AS FirstActor,
       A2.Name AS SecondActor,
	   A1.Id AS FirstActorId,
	   A2.Id AS SecondActorId,
       COUNT(*)OVER(PARTITION BY A1.Id,A2.Id) TotalFileCount
--DROP TABLE IF EXISTS ActorPairs
INTO ActorPairs
FROM Actors AS A1 
INNER JOIN Actors A2 
ON A1.Id!=A2.Id
INNER JOIN MovieActorMapping AS MA1 
ON MA1.ActorId=A1.Id
INNER JOIN MovieActorMapping AS MA2 
ON MA2.MovieId=MA1.MovieId AND MA2.ActorId=A2.Id
INNER JOIN Movies AS M 
ON M.Id=MA1.MovieId
WHERE A1.Id>A2.Id
ORDER BY TotalFileCount DESC

SELECT FirstActor,SecondActor,COUNT(*) AS COUNT
FROM ActorPairs
WHERE TotalFileCount>1
GROUP BY FirstActor,SecondActor

--other method
SELECT A1.Name,A2.Name,COUNT(MA1.MovieId)
FROM Actors A1
JOIN MovieActorMapping MA1
ON A1.Id=MA1.ActorId
JOIN MovieActorMapping MA2
ON MA1.MovieId=MA2.MovieId
JOIN Actors A2
ON A2.Id=MA2.ActorId
WHERE A1.Id<a2.Id
GROUP BY A1.Name,A2.Name
HAVING COUNT(MA1.MovieId) > 1


--Add non-clustered index on profit column of movies table
CREATE NONCLUSTERED INDEX Profit_Index
ON Movies(Profit )

SELECT * FROM MOVIES

--Create stored procedure to return list of actors for given movie id
GO
CREATE PROCEDURE ActorsList (@MovieId INT)
AS
SELECT A.Name AS [ActorNames] 
FROM MovieActorMapping MA
JOIN Actors A
ON MA.ActorId=A.Id
WHERE MA.MovieId=@MovieId
--execution
EXEC ActorsList 2

--Create a function to return age for given date of birth
GO
CREATE FUNCTION AGE (@DOB AS DATE)
RETURNS INT
AS
BEGIN
	DECLARE @Age AS INT
	SET @Age = FLOOR(DATEDIFF(DAY, @DOB , getdate()) / 365.25)
	RETURN @Age
END
GO
--execution
SELECT dbo.AGE('2000-05-25') AS age

--Create a stored procedure to increase the profit (+100) of movies with given Ids (comma separated) 
GO
CREATE PROCEDURE MovieProfits
@Ids VarChar(20)
AS
SET @Ids=','+Replace(@Ids,' ', '')+',';
UPDATE Movies SET Profit+=1000
where Charindex(','+cast(Id as varchar(800))+',', @Ids) > 0

--CALLING
EXEC MovieProfits '1,2,3'