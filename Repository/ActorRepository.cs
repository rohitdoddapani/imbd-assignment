using System;
using System.Collections.Generic;
using Dapper;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace IMDBAPI.Repository
{
    public class ActorRepository : IActorRepository
    {
        private readonly Connection _connection;

        public ActorRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }
        void IActorRepository.Delete(int id)
        {
            const string query = @"
                DELETE FROM MovieActorMapping WHERE ActorId=@ActorId
                DELETE FROM ACTORS WHERE ID=@ActorId";

            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query, new
            {
                ActorId = id
            });
        }

        IEnumerable<Actor> IActorRepository.Get()
        {
            const string sql = @"
                        SELECT *
                        FROM Actors";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var actors = connection.Query<Actor>(sql);
                return actors;
            }
        }

        IEnumerable<Actor> IActorRepository.GetActorsByMovieId(int id)
        {
            const string sql = @"
                        SELECT A.Id [Id]
                        ,A.Name [Name]
                        ,A.Bio [Bio]
                        ,A.DOB [DOB]
                        ,A.Gender [Gender]
                        FROM Actors A
                        JOIN MovieActorMapping MA
                        ON A.Id=MA.ActorId
                        WHERE MA.MovieId=@MovieId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var actors = connection.Query<Actor>(sql, new
            {
                MovieId = id
            });
                return actors;
            }
        }

        Actor IActorRepository.Get(int id)
        {
            const string sql = @"
                        SELECT *
                        FROM Actors
                        WHERE Id=@ActorId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var actors = connection.QuerySingleOrDefault<Actor>(sql,new {
                    ActorId = id
                });
                return actors;
            }
        }

        void IActorRepository.Post(ActorRequest actor)
        {
            const string query = 
                    @"
                    INSERT INTO [dbo].[Actors]
                    (
                        [Name]
                        ,[Gender]
                        ,[DOB]
                        ,[Bio]
                    )
                    VALUES 
                    (
                        @Name
                        ,@Gender
                        ,@DOB
                        ,@Bio
                    )";
            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query, new
            {
                Name = actor.Name,
                Gender = actor.Gender,
                DOB = actor.DOB,
                Bio = actor.Bio
            });
        }

        void IActorRepository.Put(int id, ActorRequest actor)
        {
            string query = 
                @"UPDATE Actors
                SET Name = @Name
                ,Gender = @Gender
                ,DOB = @DOB
                ,Bio = @Bio
                WHERE Actors.ID = @Id";
            using var connection = new SqlConnection(_connection.DB);
            var actors = connection.Execute(query, new
            {
                Id=id,
                Name = actor.Name,
                Gender = actor.Gender,
                DOB = actor.DOB,
                Bio = actor.Bio
            });
            
        }
    }
}