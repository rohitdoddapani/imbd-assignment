using System.Collections.Generic;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;

namespace IMDBAPI.Repository
{
    public interface IMovieRepository
    {
        public IEnumerable<Movie> Get();
        
        public Movie Get(int id);

        public void Post(MovieRequest movie);
        public void Put(int id,MovieRequest movie);

        public void Delete(int id);

        public IEnumerable<Review> GetReviews(int id);
    }
}