using System;
using System.Collections.Generic;
using Dapper;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace IMDBAPI.Repository
{
    public class GenreRepository : IGenreRepository
    {
        private readonly Connection _connection;

        public GenreRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }

        void IGenreRepository.Delete(int id)
        {
            const string query = @"
                DELETE FROM MovieGenreMapping WHERE GenreId=@GenreId
                DELETE FROM Genres WHERE ID=@GenreId";

            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query, new
            {
                GenreId = id
            });
        }

        IEnumerable<Genre> IGenreRepository.Get()
        {
            const string sql = @"
                        SELECT *
                        FROM Genres";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var genres = connection.Query<Genre>(sql);
                return genres;
            }
            //return _genres;
        }

        IEnumerable<Genre> IGenreRepository.GetGenresByMovieId(int id)
        {
            const string sql = @"
                        SELECT G.Id [Id],
                        G.Name [Name]
                        FROM Genres G
                        JOIN MovieGenreMapping MA
                        ON G.Id=MA.GenreId
                        WHERE MA.MovieId=@MovieId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var genres = connection.Query<Genre>(sql, new
                {
                    MovieId = id
                });
                return genres;
            }
        }
        Genre IGenreRepository.Get(int id)
        {
            const string sql = @"
                        SELECT *
                        FROM Genres
                        WHERE Id=@GenreId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var genres = connection.QuerySingleOrDefault<Genre>(sql,new {
                    GenreId = id
                });
                return genres;
            }
        }

        void IGenreRepository.Post(GenreRequest genre)
        {
            const string query = 
                    @"
                    INSERT INTO Genres
                    (
                        [Name]
                    )
                    VALUES 
                    (
                        @Name
                    )";
            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query, new
            {
                Name = genre.Name
            });
        }

        void IGenreRepository.Put(int id, GenreRequest genre)
        {
            string query = 
                @"UPDATE Genres
                SET Name = @Name
                WHERE Genres.ID = @Id";
            using var connection = new SqlConnection(_connection.DB);
            var genres = connection.Execute(query, new
            {
                Id=id,
                Name = genre.Name,
            });
        }
    }
}