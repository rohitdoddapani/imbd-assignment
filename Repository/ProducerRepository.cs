using System;
using System.Collections.Generic;
using Dapper;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace IMDBAPI.Repository
{
    public class ProducerRepository : IProducerRepository
    {
        private readonly Connection _connection;

        public ProducerRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }

        void IProducerRepository.Delete(int id)
        {
            const string query =
                    @"DELETE FROM Movies WHERE Movies.ProducerID=@ProducerId
                    DELETE FROM PRODUCERS WHERE ID=@ProducerId";

            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query, new
            {
                ProducerId = id
            });
        }

        IEnumerable<Producer> IProducerRepository.Get()
        {
            const string sql = @"
                        SELECT *
                        FROM Producers";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var producers = connection.Query<Producer>(sql);
                return producers;
            }
            //return _producers;
        }

        Producer IProducerRepository.Get(int id)
        {
            const string sql = @"
                        SELECT Id [Id],
                        Name [Name],
                        Bio [Bio],
                        DOB [DOB],
                        Gender [Gender]
                        FROM Producers
                        WHERE Id = @MovieId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var producer = connection.QuerySingleOrDefault<Producer>(sql, new
                {
                    MovieId = id
                });
                return producer;
            }
        }

        void IProducerRepository.Post(ProducerRequest producer)
        {
            const string query = 
                    @"
                    INSERT INTO Producers
                    (
                        [Name]
                        ,[Gender]
                        ,[DOB]
                        ,[Bio]
                    )
                    VALUES 
                    (
                        @Name
                        ,@Gender
                        ,@DOB
                        ,@Bio
                    )";
            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query, new
            {
                Name = producer.Name,
                Gender = producer.Gender,
                DOB = producer.DOB,
                Bio = producer.Bio
            });
        }

        void IProducerRepository.Put(int id, ProducerRequest producer)
        {
            string query = 
                @"UPDATE Producers
                SET Name = @Name
                ,Gender = @Gender
                ,DOB = @DOB
                ,Bio = @Bio
                WHERE ID = @Id";
            using var connection = new SqlConnection(_connection.DB);
            var actors = connection.Execute(query, new
            {
                Id=id,
                Name = producer.Name,
                Gender = producer.Gender,
                DOB = producer.DOB,
                Bio = producer.Bio
            });
        }
    }
}