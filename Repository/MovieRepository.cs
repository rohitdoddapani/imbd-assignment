using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Models.Response;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace IMDBAPI.Repository
{
    public class MovieRepository : IMovieRepository
    {
        private readonly Connection _connection;

        public MovieRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }
        
        public IEnumerable<Movie> Get()
        {
            //return _movies;
            const string sql = @"
                        SELECT *
                        FROM Movies";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var movies = connection.Query<Movie>(sql);
                return movies;
            }
        }

        Movie IMovieRepository.Get(int id)
        {
            const string sql = @"
                        SELECT *
                        FROM Movies
                        WHERE Id=@MovieId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var movies = connection.QuerySingleOrDefault<Movie>(sql, new {
                    MovieId = id
                });
                return movies;
            }
        }

        public void Post(MovieRequest movie){
            using var connection = new SqlConnection(_connection.DB);
            const string procedure = @"usp_AddMovie";
            var values = new
            {
                Name = movie.Name,
                Year = movie.Year,
                Plot = movie.Plot,
                ProducerId = movie.ProducerId,
                Poster = movie.Poster,
                ActorIds = string.Join(',', movie.Actors),
                GenreIds = string.Join(',', movie.Genres)

            };
            connection.Execute(procedure, values, commandType: CommandType.StoredProcedure);
        }

        public void Put(int id,MovieRequest movie){
            using var connection = new SqlConnection(_connection.DB);
            const string procedure = @"usp_UpdateMovie";
            var values = new
            {
                ID = id,
                Name = movie.Name,
                Year = movie.Year,
                Plot = movie.Plot,
                ProducerId = movie.ProducerId,
                Poster = movie.Poster,
                ActorIds = string.Join(',', movie.Actors),
                GenreIds = string.Join(',', movie.Genres)

            };
            connection.Execute(procedure, values, commandType: CommandType.StoredProcedure);
        }

        public void Delete(int id){
            const string query = @"
                DELETE FROM MovieActorMapping WHERE MovieId=@MovieId

                DELETE FROM MovieGenreMapping WHERE MovieId=@MovieId

                DELETE FROM Movies WHERE ID=@MovieId";

            using var connection = new SqlConnection(_connection.DB);
            var result = connection.Execute(query, new
            {
                MovieId = id
            });
        }

        public IEnumerable<Review> GetReviews(int id){
            const string sql = @"
                        SELECT R.ID [Id],
                        R.Review [ReviewText]
                        FROM Reviews R
                        JOIN MovieReviewMapping MR
                        ON R.Id=MR.ReviewId
                        WHERE MR.MovieId=@MovieId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var reviews = connection.Query<Review>(sql, new
                {
                    MovieId = id
                });
                return reviews;
            }
        }
    }
}