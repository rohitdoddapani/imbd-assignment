using System.Collections.Generic;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;

namespace IMDBAPI.Repository
{
    public interface IActorRepository
    {
        public IEnumerable<Actor> Get();
        public IEnumerable<Actor> GetActorsByMovieId(int id);
        public Actor Get(int id);

        public void Post(ActorRequest actor);
        public void Put(int id,ActorRequest actor);

        public void Delete(int id);
    }
}