using System.Collections.Generic;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;

namespace IMDBAPI.Repository
{
    public interface IReviewRepository
    {
        public IEnumerable<Review> Get(int movieId);
        
        public Review Get(int movieId,int reviewId);

        public void Post(int movieId,ReviewRequest review);
        public void Put(int movieId,int reviewId,ReviewRequest review);

        public void Delete(int movieId,int reviewId);
    }
}