using System;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IMDBAPI.Repository
{
    [ApiController]
    [Route("api/producers")]
    public class ProducerController : ControllerBase
    {
        private readonly IProducerService _producerService;

        public ProducerController(IProducerService producerService)
        {
            _producerService = producerService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_producerService.Get());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var producer = _producerService.Get(id);
                if (producer == null)
                {
                    return NotFound("Producer not found");
                }
                return Ok(producer);

            }
            catch (Exception)
            {
                return NotFound("Producer not found");
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] ProducerRequest producer)
        {
            try
            {
                _producerService.Post(producer);
                return StatusCode(StatusCodes.Status201Created);

            }
            catch (Exception)
            {
                return BadRequest("Producer can't be created");
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ProducerRequest producer)
        {
            try
            {
                _producerService.Put(id, producer);
                return StatusCode(StatusCodes.Status200OK);

            }
            catch (Exception)
            {
                return BadRequest("Producer can't be updated");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _producerService.Delete(id);
                return StatusCode(StatusCodes.Status200OK);

            }
            catch (System.Exception)
            {
                return BadRequest("Producer can't be deleted");
            }
        }
    }
}