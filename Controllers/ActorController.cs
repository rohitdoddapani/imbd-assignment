using System;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IMDBAPI.Repository
{
    [ApiController]
    [Route("api/actors")]
    public class ActorController : ControllerBase
    {
        private readonly IActorService _actorService;

        public ActorController(IActorService actorService)
        {
            _actorService = actorService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_actorService.Get());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var actor = _actorService.Get(id);
                return Ok(actor);
            }
            catch (Exception)
            {
                return NotFound("Actor not found");
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]ActorRequest actor)
        {
            try{
                _actorService.Post(actor);
                return StatusCode(StatusCodes.Status201Created);
            }catch(Exception){
                return BadRequest("Actor can't be created");
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id,[FromBody]ActorRequest actor){
            try
            {
                _actorService.Put(id, actor);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                return BadRequest("Actor update failed");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id){
            try
            {
                _actorService.Delete(id);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest("Actor can't be deleted");
            }
        }
    }
}
