﻿using DemoApp.Test.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;

namespace DemoApp.Test.StepFiles
{
    [Scope(Feature = "Movie Resource")]
    [Binding]
    public class MovieSteps : BaseSteps
    {
        public MovieSteps(CustomWebApplicationFactory factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    // Mock Repo
                    services.AddScoped(_ => MovieMock.MovieRepoMock.Object);
                    services.AddScoped(service => ActorMock.ActorRepoMock.Object);
                    services.AddScoped(service => ProducerMock.ProducerRepoMock.Object);
                    services.AddScoped(service => GenreMock.GenreRepoMock.Object);
                    services.AddScoped(service => MovieMock.MovieRepoMock.Object);
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            MovieMock.MockGetAll();
            MovieMock.MockGet();
            MovieMock.MockPost();
            MovieMock.MockPut();
            MovieMock.MockDelete();
            ActorMock.MockGetActorsByMovieId();
            GenreMock.MockGetGenresByMovieId();
            ProducerMock.MockGet();
            ActorMock.MockGet();
            GenreMock.MockGet();
        }
    }
}