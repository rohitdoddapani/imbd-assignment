﻿Feature: Review Resource


Scenario: Get All Review By MovieId
	Given I am a client
	When I make GET Request 'api/movies/1/reviews'
	Then response code must be '200'
	And response data must look like '[{"id":1,"review":"R1"}]'

Scenario: Get Review By Id
	Given I am a client
	When I make GET Request 'api/movies/1/reviews/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"review":"R1"}'

Scenario: Create Review
	Given I am a client
	When I am making a post request to 'api/movies/1/reviews' with the following Data '{"id":1,"review":"R1"}'
	Then response code must be '201'

Scenario: Update Review
	Given I am a client
	When I make PUT Request 'api/movies/1/reviews/1' with the following Data with the following Data '{"id":1,"review":"R1 updated"}'
	Then response code must be '200'

Scenario: Delete Review
	Given I am a client
	When I make Delete Request 'api/movies/1/reviews/1'
	Then response code must be '200'

Scenario:Update Review with Invalid id
	Given I am a client
	When I make PUT Request 'api/movies/1/reviews/2' with the following Data with the following Data '{"id":2,"review":"R1 updated"}'
	Then response code must be '400'
	And response data must look like 'review update failed'