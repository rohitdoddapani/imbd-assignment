﻿Feature: Producer Resource

Scenario: Get All Producers 
	Given I am a client
	When I make GET Request 'api/producers'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"MP1","bio":"MP1 Bio","dob":"1988-04-12T00:00:00","gender":"Male"}]'

Scenario: Get Producer By Id
	Given I am a client
	When I make GET Request 'api/producers/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"MP1","bio":"MP1 Bio","dob":"1988-04-12T00:00:00","gender":"Male"}'

Scenario: Create Producer
	Given I am a client
	When I am making a post request to 'api/producers' with the following Data '{"id":2,"name":"MP2","bio":"MP2 Bio","dob":"1989-09-02T00:00:00","gender":"Male"}'
	Then response code must be '201'

Scenario: Update Producer
	Given I am a client
	When I make PUT Request 'api/producers/1' with the following Data with the following Data '{"id":1,"name":"MP1","bio":"MP1 Bio","dob":"1988-04-12T00:00:00","gender":"Female"}'
	Then response code must be '200'

Scenario: Delete Producer
	Given I am a client
	When I make Delete Request 'api/producers/1'
	Then response code must be '200'

Scenario:Get Producer With Wrong Id
	Given I am a client
	When I make GET Request 'api/producers/3'
	Then response code must be '404'
	And response data must look like 'Producer not found'

Scenario:Update Producer With Wrong Id
	Given I am a client
	When I make PUT Request 'api/producers/2' with the following Data with the following Data '{"id":2,"name":"Jack","dateOfBirth":"1998-09-10T00:00:00","gender":"MALE ","bio":"great producer"}'
	Then response code must be '400'
	And response data must look like 'Producer can't be updated'

Scenario:Delete Producer With Wrong Id
	Given I am a client
	When I make Delete Request 'api/producers/2'
	Then response code must be '400'
	And response data must look like 'Producer can't be deleted'