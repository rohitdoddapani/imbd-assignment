﻿Feature: Genre Resource

Scenario: Get Genre All
	Given I am a client
	When I make GET Request 'api/genres'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"MG1"},{"id":2,"name":"MG2"}]'

Scenario: Get Genre By Id
	Given I am a client
	When I make GET Request 'api/genres/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"MG1"}'

Scenario: Create Genre
	Given I am a client
	When I am making a post request to 'api/genres' with the following Data '{"id":3,"name":"MG3"}'
	Then response code must be '201'

Scenario: Update Genre
	Given I am a client
	When I make PUT Request 'api/genres/1' with the following Data with the following Data '{"id":1,"name":"MG1 updated"}'
	Then response code must be '200'

Scenario: Delete Genre
	Given I am a client
	When I make Delete Request 'api/genres/1'
	Then response code must be '200'

Scenario:Get Genre With Wrong Id
	Given I am a client
	When I make GET Request 'api/genres/3'
	Then response code must be '404'
	And response data must look like 'Genre not found'

Scenario:Update Genre With Wrong Id
	Given I am a client
	When I make PUT Request 'api/genres/3' with the following Data with the following Data '{"id":3,"name":"MG3"}'
	Then response code must be '400'
	And response data must look like 'Genre can't be updated'

Scenario:Delete Genre With Wrong Id
	Given I am a client
	When I make Delete Request 'api/genres/3'
	Then response code must be '400'
	And response data must look like 'Genre can't be deleted'