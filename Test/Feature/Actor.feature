﻿Feature: Actor Resource

Scenario: Get Actor All
	Given I am a client
	When I make GET Request '/api/actors'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"MA1","bio":"MA1 Bio","dob":"1998-07-20T00:00:00","gender":"Male"},{"id":2,"name":"MA2","bio":"MA2 Bio","dob":"1996-05-21T00:00:00","gender":"Female"}]'

Scenario: Get Actor By Id
	Given I am a client
	When I make GET Request 'api/actors/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"MA1","bio":"MA1 Bio","dob":"1998-07-20T00:00:00","gender":"Male"}'

Scenario: Get Actor By MovieId
	Given I am a client
	When I make GET Request 'api/actors/movie/1'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"MA1","bio":"MA1 Bio","dob":"1998-07-20T00:00:00","gender":"Male"}]'

Scenario: Create Actor
	Given I am a client
	When I am making a post request to 'api/actors' with the following Data '{"id":3,"name":"MA3","bio":"MA3 Bio","dob":"1995-01-10T00:00:00","gender":"Male"}'
	Then response code must be '201'

Scenario: Update Actor
	Given I am a client
	When I make PUT Request 'api/actors/1' with the following Data with the following Data '{"name":"MA1 Updated","bio":"MA1 Bio","dob":"1998-07-20T00:00:00","gender":"Female"}'
	Then response code must be '200'

Scenario: Delete Actor
	Given I am a client
	When I make Delete Request 'api/actors/1'
	Then response code must be '200'

Scenario:Get Actor With Wrong Id
	Given I am a client
	When I make GET Request 'api/actors/3'
	Then response code must be '404'
	And response data must look like 'Actor not found'

Scenario:Get Actors With Wrong MovieId
	Given I am a client
	When I make GET Request 'api/actors/movie/3'
	Then response code must be '404'
	And response data must look like 'Actors not found for movie'

Scenario:Update Actor With Wrong Id
	Given I am a client
	When I make PUT Request 'api/actors/3' with the following Data with the following Data '{"name":"MA1 Updated","bio":"MA1 Bio","dob":"1998-07-20T00:00:00","gender":"Female"}'
	Then response code must be '400'
	And response data must look like 'Actor update failed'

Scenario:Delete Actor With Wrong Id
	Given I am a client
	When I make Delete Request 'api/actors/3'
	Then response code must be '400'
	And response data must look like 'Actor can't be deleted'
