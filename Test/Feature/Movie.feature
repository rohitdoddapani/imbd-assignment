﻿Feature: Movie Resource


Scenario: Get Movie All
	Given I am a client
	When I make GET Request 'api/movies'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"M1","year":2020,"plot":"M1 plot","actors":[{"id":1,"name":"MA1","bio":"MA1 Bio","dob":"1998-07-20T00:00:00","gender":"Male"}],"genres":[{"id":1,"name":"MG1"}],"producer":{"id":1,"name":"MP1","bio":"MP1 Bio","dob":"1988-04-12T00:00:00","gender":"Male"},"poster":"m1.png"}]'

Scenario: Get Movie By Id
	Given I am a client
	When I make GET Request 'api/movies/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"M1","year":2020,"plot":"M1 plot","actors":[{"id":1,"name":"MA1","bio":"MA1 Bio","dob":"1998-07-20T00:00:00","gender":"Male"}],"genres":[{"id":1,"name":"MG1"}],"producer":{"id":1,"name":"MP1","bio":"MP1 Bio","dob":"1988-04-12T00:00:00","gender":"Male"},"poster":"m1.png"}'

Scenario: Create Movie
	Given I am a client
	When I am making a post request to 'api/movies' with the following Data '{"name":"M2","Year":"2018","plot":"M2 plot","actors":[1],"genres":[1],"producerid":1,"poster":"m2.jpeg"}'
	Then response code must be '201'

Scenario: Update Movie
	Given I am a client
	When I make PUT Request 'api/movies/1' with the following Data with the following Data '{"id":1,"name":"M1 updated","Year":"2018","plot":"M1 plot","actors":[1],"genres":[1],"producerid":1,"poster":"m1.jpeg"}'
	Then response code must be '200'

Scenario: Delete Movie
	Given I am a client
	When I make Delete Request 'api/movies/1'
	Then response code must be '200'

Scenario:Get Movie For Invalid Id
	Given I am a client
	When I make GET Request 'api/movies/2'
	Then response code must be '404'
	And response data must look like 'Movie not found'

Scenario:Create Movie with Invalid ActorId
	Given I am a client
	When I am making a post request to 'api/movies' with the following Data '{"id":2,"name":"M2","Year":"2018","plot":"M2 plot","actors":[3],"genres":[1],"producerid":1,"poster":"m2.jpeg"}'
	Then response code must be '400'
	And response data must look like 'movie creation failed'

Scenario:Create Movie with Invalid ProducerId
	Given I am a client
	When I am making a post request to 'api/movies' with the following Data '{"id":2,"name":"M2","Year":"2018","plot":"M2 plot","actorids":[1],"genreids":[1],"producerid":3,"poster":"m2.jpeg"}'
	Then response code must be '400'
	And response data must look like 'movie creation failed'

Scenario:Create Movie with Invalid GenreId
	Given I am a client
	When I am making a post request to 'api/movies' with the following Data '{"id":2,"name":"M2","Year":"2018","plot":"M2 plot","actorids":[1],"genreids":[3],"producerid":1,"poster":"m2.jpeg"}'
	Then response code must be '400'
	And response data must look like 'movie creation failed'

Scenario:Update Movie with Invalid id
	Given I am a client
	When I make PUT Request 'api/movies/3' with the following Data with the following Data '{"id":3,"name":"M1 updated","Year":"2018","plot":"M1 plot","actorids":[1],"genreids":[1],"producerid":1,"poster":"m1.jpeg"}'
	Then response code must be '400'
	And response data must look like 'movie update failed'

Scenario:Delete Movie with Invalid id
	Given I am a client
	When I make Delete Request 'api/movies/10'
	Then response code must be '404'
	And response data must look like 'movie delete failed'
