﻿using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Repository;
using Moq;

namespace DemoApp.Test.MockResources
{
    public class ReviewMock
    {
        /// <summary>
        /// See how we are using Moq - https://github.com/moq/moq4
        /// </summary>
        public static readonly Mock<IReviewRepository> ReviewRepoMock = new();

        private static readonly IEnumerable<Review> ListOfReviews = new List<Review>
        {
            new Review
            {
                Id = 1,
                ReviewText = "R1",
            }
        };

        public static void MockGetAll()
        {
            ReviewRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns(ListOfReviews);
        }

        public static void MockGet()
        {
            ReviewRepoMock.Setup(repo => repo.Get(It.IsAny<int>(), It.IsAny<int>())).Returns((int movieId,int reviewId) =>
            {
                var reviewIds = MovieReviewMapping(movieId,reviewId);
                return reviewIds == -1 ? null : ListOfReviews.SingleOrDefault(x => x.Id == reviewIds);
            });
        }

        public static void MockPost()
        {
            ReviewRepoMock.Setup(repo => repo.Post(It.IsAny<int>(), It.IsAny<ReviewRequest>()));
        }
        public static void MockPut()
        {
            ReviewRepoMock.Setup(repo => repo.Put(It.IsAny<int>(),It.IsAny<int>(), It.IsAny<ReviewRequest>()));
        }
        public static void MockDelete()
        {
            ReviewRepoMock.Setup(repo => repo.Delete(It.IsAny<int>(),It.IsAny<int>()));
        }


        private static int MovieReviewMapping(int id,int reviewId)
        {
            var MovieReviewDict = new Dictionary<int, List<int>>();
            var listOfReviewId = new List<int>() { 1 };
            MovieReviewDict.Add(1, listOfReviewId);
            if (MovieReviewDict.ContainsKey(id))
            {
                if (MovieReviewDict[id].Contains(reviewId))
                {
                    foreach (var val in MovieReviewDict[id])
                    {
                        if (val == reviewId)
                        {
                            return val;
                        }
                    }

                    return -1;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }


    }
}