﻿using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Repository;
using Moq;

namespace DemoApp.Test.MockResources
{
    public class GenreMock
    {
        /// <summary>
        /// See how we are using Moq - https://github.com/moq/moq4
        /// </summary>
        public static readonly Mock<IGenreRepository> GenreRepoMock = new Mock<IGenreRepository>();

        private static readonly IEnumerable<Genre> ListOfGenres = new List<Genre>
        {
            new Genre
            {
                Id = 1,
                Name = "MG1"
            },
            new Genre
            {
                Id = 2,
                Name = "MG2"
            }
        };

        public static void MockGetAll()
        {
            GenreRepoMock.Setup(x => x.Get()).Returns(ListOfGenres);
        }

        public static void MockGet()
        {
            GenreRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListOfGenres.SingleOrDefault(x => x.Id == id));
        }

        public static void MockPost()
        {
            GenreRepoMock.Setup(repo => repo.Post(It.IsAny<GenreRequest>()));
        }
        public static void MockPut()
        {
            GenreRepoMock.Setup(repo => repo.Put(It.IsAny<int>(), It.IsAny<GenreRequest>()));
        }
        public static void MockDelete()
        {
            GenreRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }


        public static void MockGetGenresByMovieId()
        {
            GenreRepoMock.Setup(repo => repo.GetGenresByMovieId(It.IsAny<int>())).Returns((int id) =>
            {
                var genreIds = MovieGenreMapping(id);
                return genreIds == null ? null : ListOfGenres.Where(obj => genreIds.Contains(obj.Id));
            });
        }

        private static List<int> MovieGenreMapping(int id)
        {
            var MovieGenreMapping = new Dictionary<int, List<int>>();
            var listOfGenresId = new List<int>() { 1 };
            MovieGenreMapping.Add(1, listOfGenresId);
            return MovieGenreMapping.ContainsKey(id) ? MovieGenreMapping[id] : null;
        }
    }
}