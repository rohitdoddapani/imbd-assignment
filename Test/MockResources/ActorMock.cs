﻿using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Repository;
using Moq;

namespace DemoApp.Test.MockResources
{
    public class ActorMock
    {
        /// <summary>
        /// See how we are using Moq - https://github.com/moq/moq4
        /// </summary>
        public static readonly Mock<IActorRepository> ActorRepoMock = new();

        private static readonly IEnumerable<Actor> ListOfActors = new List<Actor>
        {
            new Actor
            {
                Id = 1,
                Name = "MA1",
                Bio = "MA1 Bio",
                DOB = new DateTime(1998,07,20),
                Gender = "Male"
            },
            new Actor
            {
                Id = 2,
                Name = "MA2",
                Bio = "MA2 Bio",
                DOB = new DateTime(1996,05,21),
                Gender = "Female"
            }
        };

        public static void MockGetAll()
        {
            ActorRepoMock.Setup(x => x.Get()).Returns(ListOfActors);
        }

        public static void MockGet()
        {
            ActorRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id)=> ListOfActors.SingleOrDefault(x=>x.Id==id));
        }

        public static void MockPost()
        {
            ActorRepoMock.Setup(repo => repo.Post(It.IsAny<ActorRequest>()));
        }
        public static void MockPut()
        {
            ActorRepoMock.Setup(repo => repo.Put(It.IsAny<int>(),It.IsAny<ActorRequest>()));
        }
        public static void MockDelete()
        {
            ActorRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }


        public static void MockGetActorsByMovieId()
        {
            ActorRepoMock.Setup(repo => repo.GetActorsByMovieId(It.IsAny<int>())).Returns((int id) =>
            {
                var actorIds = MovieActorMapping(id);
                return actorIds == null ? null : ListOfActors.Where(obj => actorIds.Contains(obj.Id));
            });
        }

        private static List<int> MovieActorMapping(int id)
        {
            var MovieActorMapping = new Dictionary<int, List<int>>();
            var listOfActorsId = new List<int>() { 1 };
            MovieActorMapping.Add(1, listOfActorsId);
            return MovieActorMapping.ContainsKey(id) ? MovieActorMapping[id] : null;
        }
    }
}