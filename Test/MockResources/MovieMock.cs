﻿using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Repository;
using Moq;

namespace DemoApp.Test.MockResources
{
    public class MovieMock
    {
        /// <summary>
        /// See how we are using Moq - https://github.com/moq/moq4
        /// </summary>
        public static readonly Mock<IMovieRepository> MovieRepoMock = new();

        private static readonly IEnumerable<Movie> ListOfMovies = new List<Movie>
        {
            new Movie
            {
                Id = 1,
                Name = "M1",
                Year = 2020,
                Plot = "M1 plot",
                Poster = "m1.png",
                ProducerId = 1
            }
        };

        public static void MockGetAll()
        {
            MovieRepoMock.Setup(x => x.Get()).Returns(ListOfMovies);
        }

        public static void MockGet()
        {
            MovieRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListOfMovies.SingleOrDefault(x => x.Id == id));
        }

        public static void MockPost()
        {
            MovieRepoMock.Setup(repo => repo.Post(It.IsAny<MovieRequest>()));
        }
        public static void MockPut()
        {
            MovieRepoMock.Setup(repo => repo.Put(It.IsAny<int>(), It.IsAny<MovieRequest>()));
        }
        public static void MockDelete()
        {
            MovieRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }


        /*private static List<int> MovieActorMapping(int id)
        {
            var MovieActorMapping = new Dictionary<int, List<int>>();
            var listOfActorsId = new List<int>() { 1 };
            MovieActorMapping.Add(1, listOfActorsId);
            return MovieActorMapping.ContainsKey(id) ? MovieActorMapping[id] : null;
        }*/


    }
}