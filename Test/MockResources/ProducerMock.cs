﻿using System;
using System.Collections.Generic;
using System.Linq;
using IMDBAPI.Model.DB;
using IMDBAPI.Model.Request;
using IMDBAPI.Repository;
using Moq;

namespace DemoApp.Test.MockResources
{
    public class ProducerMock
    {
        /// <summary>
        /// See how we are using Moq - https://github.com/moq/moq4
        /// </summary>
        public static readonly Mock<IProducerRepository> ProducerRepoMock = new Mock<IProducerRepository>();

        private static readonly IEnumerable<Producer> ListOfProducers = new List<Producer>
        {
            new Producer
            {
                Id = 1,
                Name = "MP1",
                Bio = "MP1 Bio",
                DOB = new DateTime(1988,04,12),
                Gender = "Male"
            }
        };

        public static void MockGetAll()
        {
            ProducerRepoMock.Setup(x => x.Get()).Returns(ListOfProducers);
        }

        public static void MockGet()
        {
            ProducerRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListOfProducers.SingleOrDefault(x => x.Id == id));
        }

        public static void MockPost()
        {
            ProducerRepoMock.Setup(repo => repo.Post(It.IsAny<ProducerRequest>()));
        }
        public static void MockPut()
        {
            ProducerRepoMock.Setup(repo => repo.Put(It.IsAny<int>(), It.IsAny<ProducerRequest>()));
        }
        public static void MockDelete()
        {
            ProducerRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }


    }
}