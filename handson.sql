--Classes (Name,Section,Number)
--Teachers (Name,DOB,Gender)
--Students (Name,DOB,Gender,ClassId)

CREATE TABLE Classes(
	Id INT PRIMARY KEY,
	Name VARCHAR(100),
	Section VARCHAR(10),
	Number INT
);
CREATE TABLE Teachers(
	Id INT PRIMARY KEY,
	Name VARCHAR(100),
	DOB DATE,
	Gender VARCHAR(10)
);
CREATE TABLE Students(
	Name VARCHAR(100),
	DOB DATE,
	Gender VARCHAR(10),
	ClassId INT FOREIGN KEY REFERENCES Classes(Id)
);
CREATE TABLE TeacherClassMapping(
	teacherId INT FOREIGN KEY REFERENCES Teachers(Id), 
	ClassId INT FOREIGN KEY REFERENCES Classes(Id)
);



INSERT INTO Classes values (1,'IX','A',201)
INSERT INTO Classes values (2,'IX','B',202)
INSERT INTO Classes values (3,'X','A',203)

INSERT INTO Teachers values (1,'Lisa Kudrow','1985/06/08','Female')
INSERT INTO Teachers values (2,'Monica Bing','1982/03/06','Female')
INSERT INTO Teachers values (3,'Chandler Bing','1978/12/17','Male')
INSERT INTO Teachers values (4,'Ross Geller','1993/01/26','Male')

INSERT INTO Students values ('Scotty Loman','2006/01/31','Male',1)
INSERT INTO Students values ('Adam Scott','2005/06/01','Male',1)
INSERT INTO Students values ('Natosha Beckles','2005/01/23','Female',2)
INSERT INTO Students values ('Lilly Page','2006/11/26','Female',2)
INSERT INTO Students values ('John Freeman','2006/06/14','Male',2)
INSERT INTO Students values ('Morgan Scott','2005/05/18','Male',3)
INSERT INTO Students values ('Codi Gass','2005/12/24','Female',3)
INSERT INTO Students values ('Nick Roll','2005/12/24','Male',3)
INSERT INTO Students values ('Dave Grohl','2005/02/12','Male',3)
INSERT INTO Students values ('bhi sikhl','2003/02/12','Male',3)

INSERT INTO TeacherClassMapping values (1,1)
INSERT INTO TeacherClassMapping values (1,2)
INSERT INTO TeacherClassMapping values (2,2)
INSERT INTO TeacherClassMapping values (2,3)
INSERT INTO TeacherClassMapping values (3,3)
INSERT INTO TeacherClassMapping values (3,1)


--Find list of male students 
SELECT * FROM Students WHERE Gender='Male';
--Find list of student older than 2005/01/01
SELECT * FROM Students WHERE DOB < '2005/01/01';
--Youngest student in school
SELECT TOP 1 * 
FROM Students 
ORDER BY DOB DESC;

--Find student distinct birthdays
SELECT DISTINCT DOB FROM Students; 

-- No of students in each class
SELECT ClassId,COUNT(*) 
FROM Students 
GROUP BY ClassId

-- No of students in each section
SELECT C.Name,C.Section,COUNT(c.Id) 
FROM Classes c
INNER JOIN Students s
ON c.Id=s.ClassId 
GROUP BY c.Section,C.Name

-- No of classes taught by teacher
SELECT teacherId,COUNT(*)
FROM TeacherClassMapping
GROUP BY teacherId

-- List of teachers teaching Class X
SELECT T.Name 
FROM TeacherClassMapping tc
JOIN Teachers t 
ON tc.teacherId=t.Id 
WHERE tc.ClassId=3;

-- Classes which have more than 2 teachers teaching
SELECT ClassId,COUNT(*)
FROM TeacherClassMapping
GROUP BY ClassId 
HAVING COUNT(teacherId)>2;

-- List of students being taught by 'Lisa'
--USING SUBQUERY
SELECT *
FROM Students S
JOIN TeacherClassMapping TC
ON TC.ClassId=S.ClassId
WHERE TC.teacherId=(SELECT Id FROM Teachers WHERE Name='Lisa Kudrow');

--USING JOINS
SELECT S.Name AS [StudentName],T.Name AS [TeacherName]
FROM Students S
JOIN TeacherClassMapping TC
ON TC.ClassId=S.ClassId
JOIN Teachers T
ON T.Id=TC.teacherId
WHERE T.Name='Lisa Kudrow'
