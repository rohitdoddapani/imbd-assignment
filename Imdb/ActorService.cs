﻿using System;
using System.Collections.Generic;
using Imdb.domain;
using Imdb.Repository;

namespace Imdb
{
    public class ActorService
    {
        private readonly ActorRepository _actorRepository;

        public ActorService()
        {
            _actorRepository = new ActorRepository();
        }

        public void AddActor(string name, DateTime dob)
        {
            if (string.IsNullOrEmpty(name) )
            {
                throw new ArgumentException("Invalid arguments");
            }

            var actor = new Actor()
            {
                Name = name,
                DateOfBirth = dob
            };

            _actorRepository.Add(actor);
        }

        public List<Actor> GetActors()
        {
            return _actorRepository.Get();
        }
    }
}