﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Imdb.domain;
using Imdb.Repository;

namespace Imdb
{
    class Program
    {
        static void Main(string[] args)
        {


            bool Flag = true;
            
            var actor1 = new Actor("Tokyo", new DateTime(1995, 04, 14));
            var actor2 = new Actor("Berlin", new DateTime(1998, 05, 30));

            var producer1 = new Producer("dvv", new DateTime(1990, 03, 19));

            /*List<Actor> listOfActors = new List<Actor>
            {
                actor1,
                actor2
            };*/

            //var movies = new Movie("el cassido", 1995, "sample plot of movie");
            //List<Movie> listOfMovies = new List<Movie>();
            //listOfMovies.Add(movies);
            /*foreach (var actor in listOfActors)
            {
                Console.WriteLine(actor.Name);
            }*/
            var actorService = new ActorService();
            var movieService = new MovieService();
            var producerService = new ProducerService();
            
            movieService.AddMovie("kgf", 1998,"sample plot",new List<Actor>(){
                actor1,
                actor2
            },producer1);
            actorService.AddActor("dfs",new DateTime(1998,05,20));
            actorService.AddActor("rbfs", new DateTime(1988, 04, 30));
            producerService.AddProducer("svcc",new DateTime(1998,03,18));

            Console.WriteLine(
                @"1. List Movies
                2.Add Movie
                3.Add Actor
                4.Add Producer
                5.Delete Movie
                6.Exit
            ");
            while (Flag==true)
            {
                int actorIterator = 1;
                int producerIterator = 1;
                int movieIterator = 1;
                var actorsList = actorService.GetActors();
                var producersList = producerService.GetProducers();
                var moviesList = movieService.GetMovies();
                Console.WriteLine("What do you want to do?");
                var selectedOption = Convert.ToInt32(Console.ReadLine());
                switch (selectedOption)
                {
                    case 1:
                        foreach (var movie in moviesList)
                        {
                            Console.WriteLine();
                            Console.WriteLine("Name:"+movie.Name);
                            Console.WriteLine("Year:"+movie.Year);
                            Console.WriteLine("Plot:"+movie.Plot);
                            Console.Write("Actors:");
                            foreach (var actor in movie.Actors)
                            {
                                Console.Write(actor.Name+',');    
                            }
                            Console.WriteLine();
                            Console.Write("Producers:");
                            Console.WriteLine(movie.Producer.Name);
                        }
                        break;
                    case 2:
                        var movieYear = new int();
                        List<Actor> movieActorsList = new List<Actor>();
                        Producer movieProducersList = new Producer();
                        Console.Write("Name:");
                        var movieName = Console.ReadLine();
                        if (string.IsNullOrEmpty(movieName))
                        {
                            Console.WriteLine("movie name must not be empty or null");
                            break;
                        }
                        Console.Write("Year:");
                        try
                        {
                            movieYear = Convert.ToInt32(Console.ReadLine());
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine(
                                $"The year entered is not valid.");
                            break;
                        }
                        Console.Write("Plot:");
                        var moviePlot = Console.ReadLine();
                        if (string.IsNullOrEmpty(moviePlot))
                        {
                            Console.WriteLine("movie plot must not be empty or null");
                            break;
                        }
                        Console.WriteLine("Choose Actors");
                        foreach (var actor in actorsList)
                        {
                            Console.WriteLine((actorIterator++) +"."+actor.Name);
                        }
                        try
                        {
                            var givenActors = Console.ReadLine().Split(' ');
                            foreach (var actorIndex in givenActors)
                            {
                                var convertedActorIndex = Convert.ToInt32(actorIndex);
                                movieActorsList.Add(actorsList[convertedActorIndex - 1]);
                            }
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("The actors choosen is not valid.");
                            break;
                        }
                        
                        Console.WriteLine("Choose Producers");
                        foreach (var producer in producersList)
                        {
                            Console.WriteLine((producerIterator++) + "." + producer.Name);

                        }
                        try
                        {
                            int producerIndex = Convert.ToInt32(Console.ReadLine());
                            movieProducersList = producersList[producerIndex - 1];
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("The producer choosen is not valid.");
                            break;
                        }
                        movieService.AddMovie(movieName,movieYear,moviePlot,movieActorsList,movieProducersList);
                        break;
                    case 3:
                        try
                        {
                            Console.Write("Name:");
                            var actorName = Console.ReadLine();
                            Console.Write("DOB:");
                            var actorDob = Convert.ToDateTime(Console.ReadLine());
                            actorService.AddActor(actorName, actorDob);
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Enter dob properly");
                        }
                        break;
                    case 4:
                        try
                        {
                            Console.Write("Name:");
                            var producerName = Console.ReadLine();
                            Console.Write("DOB:");
                            var producerDob = Convert.ToDateTime(Console.ReadLine());
                            producerService.AddProducer(producerName, producerDob);
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Enter dob properly");
                        }
                        break;
                    case 5:
                        Console.WriteLine("Choose Movie to delete");
                        foreach (var movie in moviesList)
                        {
                            Console.WriteLine((movieIterator++) + "." + movie.Name);
                        }

                        try
                        {
                            var movieNameIndexToDelete = Convert.ToInt32(Console.ReadLine());
                            var movieNameToDelete = moviesList[movieNameIndexToDelete - 1].Name;
                            movieService.DeleteMovie(movieNameToDelete);
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Enter movie index properly");
                        }
                        break;
                    case 6:
                        Flag = false;
                        break;
                    default: Console.WriteLine("Enter correct option");
                        break;
                }
            }
        }
    }
}
