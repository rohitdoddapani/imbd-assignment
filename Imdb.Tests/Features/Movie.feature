﻿Feature: Movie
	Simple imdb like project for adding and displaying movies

@listMovie
Scenario: List movie
	Given the list of movies
	When the fetch the movies
	Then the result should show like this
	| Name | Year | Plot         | Actors       | Producers |
	| kgf  | 1998 | sample plot  | Tokyo,Berlin | dvv       |
	| bb2  | 2018 | plot for bb2 | Denver       | svcc      |

@addMovie
Scenario: Add movie
	Given the name of the movie is 'z1'
	And the year of release is '2017'
	And plot of movie is 'plot text for movie z1'
	And choose actors '1,2'
	And choose producer '2'
	When the movie is added
	Then movie list should look like this
	| Name | Year | Plot                   | Actors       | Producers |
	| kgf  | 1998 | sample plot            | Tokyo,Berlin | dvv       |
	| bb2  | 2018 | plot for bb2           | Denver       | svcc      |
	| z1   | 2017 | plot text for movie z1 | Tokyo,Berlin | svcc      |