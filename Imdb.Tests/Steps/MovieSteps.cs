﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using FluentAssertions.Collections;
using Imdb.domain;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Imdb.Tests.Features
{
    [Binding]
    public class MovieSteps
    {
        private readonly MovieService _movieService;
        private readonly ActorService _actorService;
        private readonly ProducerService _producerService;
        private List<Movie> _listOfMovies;
        private readonly List<Actor> _actors = new List<Actor>();
        private Producer _producers = new Producer();
        private string movieName;
        private int movieYear;
        private string moviePlot;

        public MovieSteps()
        {
            _movieService = new MovieService();
            _actorService = new ActorService();
            _producerService = new ProducerService();
        }

        [Given(@"the list of movies")]
        public void GivenTheListOfMovies()
        {
            
        }

        [When(@"the fetch the movies")]
        public void WhenTheFetchTheMovies()
        {
            _listOfMovies = _movieService.GetMovies();
        }


        [Then(@"the result should show like this")]
        public void ThenTheResultShouldShowLikeThis(Table table)
        {
            /*var actorsList = _listOfMovies[0].Actors;
            var data = table.CreateInstance<Movie>();
            *//*data.Name.Equals(_listOfMovies[0].Name);
            var actorsToCompare = data.Actors;*//*
            int index = 0;
            foreach (var movie in _listOfMovies)
            {
                data.Name.CompareTo(movie.Name);
                data.Year.Equals(movie.Year);
                data.Plot.Equals(data.Plot);
                foreach (var actor in movie.Actors)
                {
                    //actor.Name.Equals(actorsToCompare[index++].Name);
                    data.Actors[index++].Equals(actor.Name);
                }
            }*/
            var movieTable = table.CreateSet<(string Name,int Year,string Plot,List<string> Actors,List<string> Producers)>();
            /*for (var j = 0; j < _listOfMovies.Count; j++)
            {
                account[j]
            }*/
            /*foreach (var i in account)
            {
                foreach (var movie in _listOfMovies)
                {
                    int index = 0;
                    i.Name.CompareTo(movie.Name);
                    foreach (var actor in movie.Actors)
                    {
                        //actor.Name.Equals(actorsToCompare[index++].Name);
                        i.Actors[index].CompareTo(movie.Actors[index].Name);
                        index++;
                    }
                    *//*i.Actors[index++].CompareTo(movie.Actors[index++].Name);*//*
                }
            }*/
            int movieIndex = 0;
            foreach (var (name, year, plot, actors, producers) in movieTable)
            {
                int index = 0;
                var localMoviesList = _listOfMovies[movieIndex];
                Assert.AreEqual(name, localMoviesList.Name);
                Assert.AreEqual(year, localMoviesList.Year);
                Assert.AreEqual(plot, localMoviesList.Plot);
                foreach (var actor in localMoviesList.Actors)
                {
                    //actor.Name.Equals(actorsToCompare[index++].Name);
                    //i.Actors[index].Equals(actor.Name);
                    Assert.AreEqual(actors[index], actor.Name);
                    index++;
                }
                movieIndex++;
            }
            //table.CompareToInstance<Movie>(_listOfMovies[0]);
        }

        [BeforeScenario("listMovie")]
        public void AddSampleMovieForList()
        {
            var actor1 = new Actor("Tokyo", new DateTime(1995, 04, 14));
            var actor2 = new Actor("Berlin", new DateTime(1998, 05, 30));
            var actor3 = new Actor("Denver", new DateTime(1988, 09, 30));

            var producer1 = new Producer("dvv", new DateTime(1990, 03, 19));
            var producer2 = new Producer("svcc", new DateTime(1992, 01, 21));

            _movieService.AddMovie("kgf", 1998, "sample plot", new List<Actor>(){
                actor1,
                actor2
            },  producer1 );
            _movieService.AddMovie("bb2", 2018, "plot for bb2", new List<Actor>(){
                actor3
            }, producer2 );
        }

        //Add Movie

        [Given(@"the name of the movie is '(.*)'")]
        public void GivenTheNameOfTheMovieIs(string p0)
        {
            movieName = p0;
        }

        [Given(@"the year of release is '(.*)'")]
        public void GivenTheYearOfReleaseIs(int p0)
        {
            movieYear = p0;
        }

        [Given(@"plot of movie is '(.*)'")]
        public void GivenPlotOfMovieIs(string p0)
        {
            moviePlot = p0;
        }

       
        [Given(@"choose actors '(.*)'")]
        public void GivenChooseActors(string actorIndexes)
        {
            var convertedActorIndexes = actorIndexes
                                .Split(',')
                                .Where(index => int.TryParse(index,out _))
                                .Select(int.Parse)
                                .ToList();
            var initialActorsList = _actorService.GetActors();
            foreach (var actorIndex in convertedActorIndexes)
            {
                _actors.Add(initialActorsList[actorIndex-1]);
            }
        }



        [Given(@"choose producer '(.*)'")]
        public void GivenChooseProducer(int producerIndex)
        {
            var initialProducersList = _producerService.GetProducers();
            _producers = initialProducersList[producerIndex-1];
        }

        [When(@"the movie is added")]
        public void WhenTheMovieIsAdded()
        {
            _movieService.AddMovie(movieName, movieYear, moviePlot, _actors, _producers);
        }

        [Then(@"movie list should look like this")]
        public void ThenMovieListShouldLookLikeThis(Table table)
        {
            var movieTable = table.CreateSet<(string Name, int Year, string Plot, List<string> Actors, List<string> Producers)>();
            var availableMovieList = _movieService.GetMovies();

            //trying linq queries
            var initialProducersList = _producerService.GetProducers();
            var producerList =
                availableMovieList.Where(m => m.Actors.Any(actor => actor.Name == "Tokyo"))
                    .Select(movie => movie.Producer.Name);
            // Selecting movies which have an actor named 'Tokyo'
            var moviesListWithTokyo = availableMovieList
                .Where(movie=>movie.Actors.Any(actor => actor.Name == "Tokyo"))
                .ToList();
            // Get Count of Actors who's name contains substring "abc"
            var initialActorsList = _actorService.GetActors();
            var actorsCount = initialActorsList
                .Where(actor => actor.Name.Contains("kyo"))
                .Select(actor => actor).Count();
            /*
                can directly use this 
            var actorsCount = initialActorsList
                .Count(actor => actor.Name.Contains("kyo"));*/

            // Get all producer names and the names of movies they have worked in

            //using linq
            
            var producerMovies = from producer in initialProducersList
                                 join movie in availableMovieList
                    on producer.Name equals movie.Producer.Name into grouping
                        select new
                {
                    producerName = grouping,
                    ProducerMovies = string.Join(",", grouping.Select(x => x.Name)),
                };

            //using foreach
            var producerdict = new Dictionary<string, List<string>>();
            foreach (var producer in initialProducersList)
            {
                foreach (var movie in availableMovieList)
                {
                    if (movie.Producer.Name == producer.Name)
                    {
                        if (producerdict.ContainsKey(producer.Name))
                        {
                            producerdict[producer.Name].Add(movie.Name);
                        }
                        else
                        {
                            producerdict[producer.Name] = new List<string>();
                            producerdict[producer.Name].Add(movie.Name);
                        }
                        
                    }
                }
            }

            Console.WriteLine(producerdict);

            int movieIndex = 0;
            foreach (var i in movieTable)
            {
                int index = 0;
                var localMoviesList = availableMovieList[movieIndex];
                Assert.AreEqual(i.Name, localMoviesList.Name);
                Assert.AreEqual(i.Year, localMoviesList.Year);
                Assert.AreEqual(i.Plot, localMoviesList.Plot);
                foreach (var actor in localMoviesList.Actors)
                {
                    //actor.Name.Equals(actorsToCompare[index++].Name);
                    //i.Actors[index].Equals(actor.Name);
                    Assert.AreEqual(i.Actors[index], actor.Name);
                    index++;
                }
                movieIndex++;
            }

        }

        [BeforeScenario("addMovie")]
        public void AddSampleMovieForAddMovie()
        {
            var actor1 = new Actor("Tokyo", new DateTime(1995, 04, 14));
            var actor2 = new Actor("Berlin", new DateTime(1998, 05, 30));
            var actor3 = new Actor("Denver", new DateTime(1988, 09, 30));

            var producer1 = new Producer("dvv", new DateTime(1990, 03, 19));
            var producer2 = new Producer("svcc", new DateTime(1992, 01, 21));

            _actorService.AddActor("Tokyo", new DateTime(1995, 04, 14));
            _actorService.AddActor("Berlin", new DateTime(1998, 05, 30));
            _actorService.AddActor("Denkyo", new DateTime(1998, 05, 30));
            _producerService.AddProducer("dvv", new DateTime(1990, 03, 19));
            _producerService.AddProducer("svcc", new DateTime(1992, 01, 21));
            _producerService.AddProducer("amb", new DateTime(1992, 01, 21));

            _movieService.AddMovie("kgf", 1998, "sample plot", new List<Actor>(){
                actor1,
                actor2
            }, producer1 );
            _movieService.AddMovie("bb2", 2018, "plot for bb2", new List<Actor>(){
                actor3
            }, producer2 );
        }
    }
}
