﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Imdb.domain;

namespace Imdb.Repository
{
    public class MovieRepository
    {
        private readonly List<Movie> _movies;

        public MovieRepository()
        {
            _movies = new List<Movie>();
        }

        public void Add(Movie movie)
        {
            _movies.Add(movie);
        }

        public void Remove(Movie movie)
        {
            _movies.Remove(movie);
        }
        public List<Movie> Get()
        {
            return _movies.ToList();
        }
    }
}
