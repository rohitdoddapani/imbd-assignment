﻿using System.Collections.Generic;
using System.Linq;
using Imdb.domain;

namespace Imdb.Repository
{
    public class ProducerRepository
    {
        private readonly List<Producer> _producers;

        public ProducerRepository()
        {
            _producers = new List<Producer>();
        }

        public void Add(Producer producer)
        {
            _producers.Add(producer);
        }

        public List<Producer> Get()
        {
            return _producers.ToList();
        }
    }
}