

$(function(){
    $("#nav-placeholder").load("nav.html");

    $(".nav-item").click(function(){
        $(this).addClass('active');
    })
});

$(function(){
    
        
    chooseActors();
    chooseGenres();
    chooseProducers();  
    
   
    
    //choose actors
    $('#addActorButton').click(function () {
        var actorName = $("#actorName").val();
        var actorBio = $("#actorBio").val();
        var actorDOB = $("#actorDOB").val();
        var actorGender = $("#actorGender").val();
        
        //console.log(actorName,actorDOB,actorGender);
        // if(actorName==''){
        //     $("#actorName").addClass("is-invalid");
        // }
        //validations
        var valid = true;
        if(actorName==''){
            $("#actorName").addClass("is-invalid");
            valid = false;
        }else{
            $("#actorName").removeClass("is-invalid");
        }
        if(actorBio==''){
            $("#actorBio").addClass("is-invalid");
            valid = false;
        }else{
            $("#actorBio").removeClass("is-invalid");
        }
        if(actorDOB==''){
            $("#actorDOB").addClass("is-invalid");
            valid = false;
        }else{
            $("#actorDOB").removeClass("is-invalid");
        }
        if(actorGender==''){
            $("#actorGender").addClass("is-invalid");
            valid = false;
        }else{
            $("#actorGender").removeClass("is-invalid");
        }

        if(valid){
            var data = {
                "name": actorName,
                "bio": actorBio,
                "dob": actorDOB,
                "gender": actorGender
            }
            
            $.ajax({ 
                url:"https://localhost:5001/api/actors",
                datatype: "json",
                type: "POST",
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function(result){
                    chooseActors();
                    $("#actorName").val('');
                    $("#actorBio").val('');
                    $("#actorDOB").val('');
                    $("#actorGender").val('');
                    $('#newActorModal').modal('toggle');
                },
                error: function(error){
                    console.log(error)
                }
            })
        }else{
            console.log("error actor")
        }
        //$('#checkActors').multiselect('rebuild');
    });
    var checkChooseOption = $("#checkActors").find().prevObject.length;
    if(checkChooseOption!=0){
        // $('#checkActors').multiselect({});
        $('#checkActors').select2();
        // $('#checkGenres').multiselect({});
        $('.genreselect').select2();
        $('.select2').select2();
    }
    //choose Genres
    $('#newGenreButton').click(function (e) {
        
        var genreTitle = $("#newGenre").val();
        
        console.log(genreTitle);
        
        var valid = true;
        if(genreTitle==''){
            $("#newGenre").addClass("is-invalid");
            valid = false;
        }else{
            $("#newGenre").removeClass("is-invalid");
        }

        if(valid){
            var data = {
                "name": genreTitle
            }
            
            $.ajax({ 
                url:"https://localhost:5001/api/genres",
                datatype: "json",
                type: "POST",
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function(result){
                    chooseGenres();
                    $("#newGenre").val('');
                    $('#newGenreModal').modal('toggle');
                },
                error: function(error){
                    console.log(error)
                }
            })
            //$('#checkGenres').multiselect('rebuild');
        }else{
            console.log("error genre");
        }
    });
    

});


function chooseActors(){
    //choose actors
    $.ajax({ 
        url:"https://localhost:5001/api/actors",
        datatype: "json",
        type: "GET",
        
        success: function(result){
            var checkChooseOption = $("#checkActors").find().prevObject.length;
            //console.log(checkChooseOption);
            if(checkChooseOption!=0){
                $("#checkActors").empty();
                result.forEach(actor => {
                    var htm = '';
                    htm += `<option value="${actor.id}"> ${actor.name} </option>`;
                    $('#checkActors').append(htm);
                    // $('#checkActors').multiselect('rebuild');
                });
            }else{
                result.forEach(actor => {
                    var actorCard = `<div class="col-md-4 col-sm-4 col-xs-6 mb-2">
                            <div class="card" >
                                <div class="card-body" id=${actor.id}>
                                <h5 class="card-title">${actor.name}</h5>
                                <p class="card-text">${actor.bio}</p>
                                <p class="card-text">DOB: ${actor.dob.slice(0,10)}</p>
                                <p class="card-text">Gender: ${actor.gender}</p>
                                <a href="#" class="btn btn-primary editActorClass" data-toggle="modal" data-target="#editActorModal">Edit</a>
                                </div>
                            </div>
                        </div>`
                        $('#actorsList').append(actorCard);
                })
            }
        },
        error: function(error){
            console.log(error)
        }
    });
}

function chooseGenres(){
    //choose genres
    $.ajax({ 
        url:"https://localhost:5001/api/genres",
        datatype: "json",
        type: "GET",
        success: function(result){
            var checkChooseOption = $("#checkGenres").find().prevObject.length;
            // console.log(checkChooseOption);
            if(checkChooseOption!=0){
                $("#checkGenres").empty();
                result.forEach(genre => {
                    var htm = '';
                    htm += `<option value=${genre.id}> ${genre.name} </option>`;
                    $('#checkGenres').append(htm);
                    // $('#checkGenres').multiselect('rebuild');
                });
            }else{
                result.forEach(genre => {
                    var genreCard = `<div class="col-md-4 col-sm-4 col-xs-6 mb-2">
                            <div class="card" >
                                <div class="card-body" >
                                <div class="row d-flex justify-content-between" id=${genre.id}>
                                <h5 class="card-title">${genre.name}</h5>
                                <a href="#" class="btn btn-primary editGenreClass" data-toggle="modal" data-target="#editGenreModal">Edit</a>
                                </div>
                                </div>
                            </div>
                        </div>`
                        $('#genresList').append(genreCard);
                })
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function chooseProducers(){
    $.ajax({ 
        url:"https://localhost:5001/api/producers",
        datatype: "json",
        type: "GET",
        success: function(result){
            $("#checkProducers").empty();
            result.forEach(producer => {
                var htm = '';
                htm += `<option value=${producer.id}> ${producer.name} </option>`;
                $('#checkProducers').append(htm);
                //$('#checkProducers').multiselect('rebuild');
                // $('.select2').select2();
            });
        },
        error: function(error){
            console.log(error)
        }
    })
}