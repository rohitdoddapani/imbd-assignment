
$(function(){
    $(document).on("click", ".editActorClass", function(e) {
        // console.log(e.target);
        // console.log(this);
        console.log(this.parentNode);
        console.log($(this.parentNode).attr('id'));
        var actorId = $(this.parentNode).attr('id');
        localStorage.setItem('actorId',actorId);
        $("#editActorButton").val("Edit Actor")
        $.ajax({ 
            url:`https://localhost:5001/api/actors/${actorId}`,
            datatype: "json",
            type: "GET",
            contentType: 'application/json',
            success: function(actor){
                $("#actorName").val(actor.name);
                $("#actorBio").val(actor.bio);
                $("#actorDOB").val(actor.dob.slice(0,10));
                $("#actorGender").val(actor.gender);
            },
            error: function(error){
                console.log(error)
            }
        })
    });

    $('#editActorButton').click(function(e){
        console.log(this);
        var actorName = $("#actorName").val();
        var actorBio = $("#actorBio").val();
        var actorDOB = $("#actorDOB").val();
        var actorGender = $("#actorGender").val();
        
        //validations
        var valid = true;
        if(actorName==''){
            $("#actorName").addClass("is-invalid");
            valid = false;
        }else{
            $("#actorName").removeClass("is-invalid");
        }
        if(actorBio==''){
            $("#actorBio").addClass("is-invalid");
            valid = false;
        }else{
            $("#actorBio").removeClass("is-invalid");
        }
        if(actorDOB==''){
            $("#actorDOB").addClass("is-invalid");
            valid = false;
        }else{
            $("#actorDOB").removeClass("is-invalid");
        }
        if(actorGender==null){
            $("#actorGender").addClass("is-invalid");
            valid = false;
        }else{
            $("#actorGender").removeClass("is-invalid");
        }

        var actorId = localStorage.getItem('actorId');
        
        var editText = $("#editActorButton").val();
        if(editText=="Add Actor"){
            console.log("add");
            if(valid){
                var data = {
                    "name": actorName,
                    "bio": actorBio,
                    "dob": actorDOB,
                    "gender": actorGender
                }
        
                $.ajax({ 
                    url:"https://localhost:5001/api/actors/",
                    datatype: "json",
                    type: "POST",
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    success: function(result){
                        $("#actorName").val('');
                        $("#actorBio").val('');
                        $("#actorDOB").val('');
                        $("#actorGender").val('');
                        $('#editActorModal').modal('toggle');
                    },
                    error: function(error){
                        console.log(error)
                    }
                })
            }else{
                //alertbox.show('Error editing movie');
            }

        }else{
            console.log("edit")
            if(valid){
                    var data = {
                        "name": actorName,
                        "bio": actorBio,
                        "dob": actorDOB,
                        "gender": actorGender
                    }
            
                    $.ajax({ 
                        url:`https://localhost:5001/api/actors/${actorId}`,
                        datatype: "json",
                        type: "PUT",
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function(result){
                            $("#actorName").val('');
                            $("#actorBio").val('');
                            $("#actorDOB").val('');
                            $("#actorGender").val('');
                            $('#editActorModal').modal('toggle');
                            location.reload()
                        },
                        error: function(error){
                            console.log(error)
                        }
                    })
            }else{
                //alertbox.show('Error editing movie');
            }
        }
    });


    $('#newActorButton').click(function(e){
        $("#editActorButton").val("Add Actor");
        $("#actorName").val('');
        $("#actorBio").val('');
        $("#actorDOB").val('');
        $("#actorGender").val('');
    });
})