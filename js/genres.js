
$(function(){
    $(document).on("click", ".editGenreClass", function(e) {
        // console.log(e.target);
        // console.log(this);
        //console.log(this.parentNode);
        console.log($(this.parentNode).attr('id'));
        var genreId = $(this.parentNode).attr('id');
        localStorage.setItem('genreId',genreId);
        $("#editGenreButton").val("Edit Genre");
        $.ajax({ 
            url:`https://localhost:5001/api/genres/${genreId}`,
            datatype: "json",
            type: "GET",
            contentType: 'application/json',
            success: function(genre){
                $("#genreName").val(genre.name);
            },
            error: function(error){
                console.log(error)
            }
        })
    });

    $('#editGenreButton').click(function(e){
        console.log(this);
        var genreName = $("#genreName").val();
        
        //validations
        var valid = true;
        if(genreName==''){
            $("#genreName").addClass("is-invalid");
            valid = false;
        }else{
            $("#genreName").removeClass("is-invalid");
        }

        var genreId = localStorage.getItem('genreId');
        
        var editText = $("#editGenreButton").val();
        if(editText=="Add Genre"){
            if(valid){
                var data = {
                    "name": genreName
                }
        
                $.ajax({ 
                    url:"https://localhost:5001/api/genres/",
                    datatype: "json",
                    type: "POST",
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    success: function(result){
                        $("#genreName").val('');
                        $('#editGenreModal').modal('toggle');
                        location.reload()
                    },
                    error: function(error){
                        console.log(error)
                    }
                })
        }else{
            //alertbox.show('Error editing movie');
        }
        }else{
            if(valid){
                    var data = {
                        "name": genreName
                    }
            
                    $.ajax({ 
                        url:`https://localhost:5001/api/genres/${genreId}`,
                        datatype: "json",
                        type: "PUT",
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function(result){
                            $("#genreName").val('');
                            $('#editGenreModal').modal('toggle');
                            location.reload()
                        },
                        error: function(error){
                            console.log(error)
                        }
                    })
            }else{
                //alertbox.show('Error editing movie');
            }
        }
    });

    $('#addGenreButton').click(function(e){
        $("#editGenreButton").val("Add Genre");
        $("#genreName").val('');
    });
})