$(function(){
 
    var editMovie = localStorage.getItem('editMovie');

    if(editMovie==1){
        console.log("In Edit");
        $("#addMovieButton").html("Edit Movie")
        $(".pageTitle").html("Edit Movie")
        var movieId = localStorage.getItem('movieId');
        $.ajax({ 
            url:`https://localhost:5001/api/movies/${movieId}`,
            datatype: "json",
            type: "GET",
            contentType: 'application/json',
            success: function(movie){
                var movieActors = [];
                var movieGenres = [];
                for (const actor of movie.actors) {
                    movieActors.push(actor.id)
                }
                for (const genre of movie.genres) {
                    movieGenres.push(genre.id);
                }
                $("#movieName").val(movie.name);
                $("#movieYOR").val(movie.year);
                $("#checkActors").val(movieActors).change();
                // $('#checkActors').multiselect('rebuild');

                $("#checkGenres").val(movieGenres).change();
                // $('#checkGenres').multiselect('rebuild');

                //$("#moviePoster").val(movie.poster);
                $('.custom-file-label').html(movie.poster);
                $("#checkProducers").val(movie.producer.id).change();
                $("#moviePlot").val(movie.plot);

            },
            error: function(error){
                console.log(error)
            }
        })
        

        
    }
    //add movie
    $('#addMovieButton').click(function(e){
        e.preventDefault();
        var editMovie = localStorage.getItem('editMovie');
        
        var movieName = $("#movieName").val();
        var movieYOR = $("#movieYOR").val();
        var movieProducer = $("#checkProducers").val();
        var movieActors = $("#checkActors").val();
        var movieGenres = $("#checkGenres").val();
        var moviePoster = $('input[type=file]')[0].files[0];
        var moviePlot = $("#moviePlot").val();
        
        console.log(movieName,movieYOR,movieActors,movieGenres,movieProducer,moviePoster,moviePlot)

        if(editMovie==1){
            var movieName = $("#movieName").val();
            var movieYOR = $("#movieYOR").val();
            var movieProducer = $("#checkProducers").val();
            var movieActors = $("#checkActors").val();
            var movieGenres = $("#checkGenres").val();
            var moviePoster = $("#moviePoster").val();
            var moviePlot = $("#moviePlot").val();

            //let moviePoster = new FormData($("#moviePoster")[0]);

            var poster = $('.custom-file-label').html();
            
            var formData = new FormData();
            formData.set('file',$('input[type=file]')[0].files[0])
            
            //validations
            var valid = true;

            if(movieName==''){
                $("#movieName").addClass("is-invalid");
                valid = false;
            }else{
                $("#movieName").removeClass("is-invalid");
            }
            if(movieYOR==''){
                $("#movieYOR").addClass("is-invalid");
                valid = false;
            }else{
                $("#movieYOR").removeClass("is-invalid");
            }
            if(movieProducer==''){
                $("#movieProducer").addClass("is-invalid");
                valid = false;
            }else{
                $("#movieProducer").removeClass("is-invalid");
            }
            if(movieActors.length==0){
                $("#checkActorsFeedback").css("display", "block");
                valid = false;
            }else{
                $("#checkActorsFeedback").css("display", "none");
            }
            if(movieGenres.length==0){
                $("#checkGenresFeedback").css("display", "block");
                valid = false;
            }else{
                $("#checkGenresFeedback").css("display", "none");
            }
            if(moviePoster===undefined){
                $("#moviePoster").addClass("is-invalid");
                valid = false;
            }else{
                $("#moviePoster").removeClass("is-invalid");
            }
            if(moviePlot==''){
                $("#moviePlot").addClass("is-invalid");
                valid = false;
            }else{
                $("#moviePlot").removeClass("is-invalid");
            }

            if(valid){
                if(moviePoster != ''){
                    $.ajax({ 
                        url:"https://localhost:5001/api/movies/upload",
                        type: "POST",
                        enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,
                        data: formData,
                        success: function(resultPoster){
                            var data = {
                                "name": movieName,
                                "year": movieYOR,
                                "plot": moviePlot,
                                "actors": movieActors,
                                "genres": movieGenres,
                                "producerId": movieProducer,
                                "poster": resultPoster
                            }
                    
                            $.ajax({ 
                                url:`https://localhost:5001/api/movies/${movieId}`,
                                datatype: "json",
                                type: "PUT",
                                data: JSON.stringify(data),
                                contentType: 'application/json',
                                success: function(result){
                                    $("#movieName").val('');
                                    $("#movieYOR").val('');
                                    $("#movieProducer").val('').change();
                                    $("#checkActors").val('').change();
                                    // $('#checkActors').multiselect('rebuild');
                                    $("#checkGenres").val('').change();
                                    // $('#checkGenres').multiselect('rebuild');
                                    $("#moviePoster").val('');
                                    $("#moviePlot").val('');
                                    $('.custom-file-label').html("choose file");
                                    $('#editMovieModal').modal('toggle');
                                    alertbox.show('Movie Edited successfully');
                                    localStorage.setItem('editMovie',0);
                                    // location.reload()
                                    location.href = 'index.html';
                                },
                                error: function(error){
                                    console.log(error)
                                }
                            })
                        },
                        error: function(error){
                            console.log(error)
                        }
                    })
                }else{
                    var data = {
                        "name": movieName,
                        "year": movieYOR,
                        "plot": moviePlot,
                        "actors": movieActors,
                        "genres": movieGenres,
                        "producerId": movieProducer,
                        "poster": poster
                    }
            
                    $.ajax({ 
                        url:`https://localhost:5001/api/movies/${movieId}`,
                        datatype: "json",
                        type: "PUT",
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        success: function(result){
                            $("#movieName").val('');
                            $("#movieYOR").val('');
                            $("#movieProducer").val('');
                            $("#checkActors").val('').change();
                            // $('#checkActors').multiselect('rebuild');
                            $("#checkGenres").val('').change();
                            // $('#checkGenres').multiselect('rebuild');
                            $("#moviePoster").val('');
                            $("#moviePlot").val('');
                            $('.custom-file-label').html("choose file");
                            $('#editMovieModal').modal('toggle');
                            alertbox.show('Movie Edited successfully');
                            localStorage.setItem('editMovie',0);
                            // location.reload()
                            location.href = 'index.html';
                        },
                        error: function(error){
                            console.log(error)
                        }
                    })
                }
            }else{
                alertbox.show('Error editing movie');
            }
        }else{
            var movieName = $("#movieName").val();
            var movieYOR = $("#movieYOR").val();
            var movieProducer = $("#checkProducers").val();
            var movieActors = $("#checkActors").val();
            var movieGenres = $("#checkGenres").val();
            var moviePoster = $('input[type=file]')[0].files[0];
            var moviePlot = $("#moviePlot").val();

            //let moviePoster = new FormData($("#moviePoster")[0]);

            //console.log(movieActors,moviePoster)

            //validations
            var valid = true;
            if(movieName==''){
                $("#movieName").addClass("is-invalid");
                valid = false;
            }else{
                $("#movieName").removeClass("is-invalid");
            }
            if(movieYOR==''){
                $("#movieYOR").addClass("is-invalid");
                valid = false;
            }else{
                $("#movieYOR").removeClass("is-invalid");
            }
            if(movieProducer==''){
                $("#movieProducer").addClass("is-invalid");
                valid = false;
            }else{
                $("#movieProducer").removeClass("is-invalid");
            }
            if(movieActors.length==0){
                $("#checkActorsFeedback").css("display", "block");
                valid = false;
            }else{
                $("#checkActorsFeedback").css("display", "none");
            }
            if(movieGenres.length==0){
                $("#checkGenresFeedback").css("display", "block");
                valid = false;
            }else{
                $("#checkGenresFeedback").css("display", "none");
            }
            if(moviePoster===undefined){
                $("#moviePoster").addClass("is-invalid");
                valid = false;
            }else{
                $("#moviePoster").removeClass("is-invalid");
            }
            if(moviePlot==''){
                $("#moviePlot").addClass("is-invalid");
                valid = false;
            }else{
                $("#moviePlot").removeClass("is-invalid");
            }
            
            var formData = new FormData();
            formData.set('file',moviePoster);

            console.log(movieGenres)

            if(valid){
                $.ajax({ 
                    url:"https://localhost:5001/api/movies/upload",
                    type: "POST",
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function(resultPoster){
                        var data = {
                            "name": movieName,
                            "year": movieYOR,
                            "plot": moviePlot,
                            "actors": movieActors,
                            "genres": movieGenres,
                            "producerId": movieProducer,
                            "poster": resultPoster
                        }
                
                        $.ajax({ 
                            url:"https://localhost:5001/api/movies",
                            datatype: "json",
                            type: "POST",
                            data: JSON.stringify(data),
                            contentType: 'application/json',
                            success: function(result){
                                $("#movieName").val('');
                                $("#movieYOR").val('');
                                $("#checkProducers").val('');
                                $("#checkActors").val('');
                                // $('#checkActors').multiselect('rebuild');
                                $("#checkGenres").val('');
                                // $('#checkGenres').multiselect('rebuild');
                                $("#moviePoster").val('');
                                $("#moviePlot").val('');
                                $('.custom-file-label').html("choose file");
                                alertbox.show('Movie Added successfully');
                                location.href = 'index.html';
                            },
                            error: function(error){
                                console.log(error)
                            }
                        })
                    },
                    error: function(error){
                        console.log(error)
                    }
                })
            }else{
                console.log("not valid");
                alertbox.show('Error adding movie');
            }
        }
    });

    
    //alert box
    var AlertBox = function(id, option) {
        this.show = function(msg) {
          if (msg === ''  || typeof msg === 'undefined' || msg === null) {
            throw '"msg parameter is empty"';
          }
          else {
            var alertArea = document.querySelector(id);
            var alertBox = document.createElement('DIV');
            var alertContent = document.createElement('DIV');
            var alertClose = document.createElement('A');
            var alertClass = this;
            alertContent.classList.add('alert-content');
            alertContent.innerText = msg;
            alertClose.classList.add('alert-close');
            alertClose.setAttribute('href', '#');
            alertBox.classList.add('alert-box');
            alertBox.appendChild(alertContent);
            if (!option.hideCloseButton || typeof option.hideCloseButton === 'undefined') {
              alertBox.appendChild(alertClose);
            }
            alertArea.appendChild(alertBox);
            alertClose.addEventListener('click', function(event) {
              event.preventDefault();
              alertClass.hide(alertBox);
            });
            if (!option.persistent) {
              var alertTimeout = setTimeout(function() {
                alertClass.hide(alertBox);
                clearTimeout(alertTimeout);
              }, option.closeTime);
            }
          }
        };
      
        this.hide = function(alertBox) {
          alertBox.classList.add('hide');
          var disperseTimeout = setTimeout(function() {
            alertBox.parentNode.removeChild(alertBox);
            clearTimeout(disperseTimeout);
          }, 500);
        };
      };
      
      var alertbox = new AlertBox('#alert-area', {
        closeTime: 5000,
        persistent: false,
        hideCloseButton: false
      });
      
})



