$(function(){

    $.ajax({ 
        url:"https://localhost:5001/api/movies",
        datatype: "json",
        type: "GET",
        
        success: function(result){
            result.forEach(
                (movie)=>{
                var movieActors = [];
                var movieGenres = [];
                for (const actor of movie.actors) {
                    movieActors.push(actor.name)
                }
                for (const genre of movie.genres) {
                    movieGenres.push(genre.name);
                }
                var movieCard = `<div class="col-md-3 col-sm-4 col-xs-6 card">
                    <div class="team" id="${movie.id}">
                        <div class="col-12 poster">
                            <img
                            src="${movie.poster}"
                            class="img-fluid rounded-start moviePoster"
                            alt="poster"
                            />
                            </div>
                            <div class="team-content">
                                <h4 class="card-title text-center movieName">${movie.name}</h4>
                                <ul class="list-group list-group-flush">
                                    
                                    <li class="list-group-item ">Actors: <span class="movieActors">${movieActors.toString()}<span></li>
                                    <li class="list-group-item">Genres: <span class="movieGenres">${movieGenres.toString()}<span></li>
                                    <li class="list-group-item ">Producer: <span class="movieProducer">${movie.producer.name}<span></li>
                                    <li class="list-group-item ">Release Year: <span class="movieProducer">${movie.year}<span></li>
                                </ul>
                                
                            </div>
                            <button class="btn btn-primary mt-2 editMovieClass" id="editMovie${movie.id}" >Edit Movie</button>
                        <!-- /.team-content -->
                    </div>
                    <!-- /.team -->
                    </div>`
                    $('#movieList').append(movieCard);
                }
            )
        },
        error: function(error){
            console.log(error)
        }
    });

    $(document).on("click", ".editMovieClass", function(e) {
        // console.log(e.target);
        // console.log(this);
        // console.log(this.parentNode);
        console.log($(this.parentNode).attr('id'));
        var movieId = $(this.parentNode).attr('id');
        localStorage.setItem('movieId',movieId);
        localStorage.setItem('editMovie',1);
        location.href = 'addMovie.html';
        // $.ajax({ 
        //     url:`https://localhost:5001/api/movies/${movieId}`,
        //     datatype: "json",
        //     type: "GET",
        //     contentType: 'application/json',
        //     success: function(movie){
        //         var movieActors = [];
        //         var movieGenres = [];
        //         for (const actor of movie.actors) {
        //             movieActors.push(actor.id)
        //         }
        //         for (const genre of movie.genres) {
        //             movieGenres.push(genre.id);
        //         }
        //         $("#movieName").val(movie.name);
        //         $("#movieYOR").val(movie.year);
        //         $("#checkActors").val(movieActors);
        //         $('#checkActors').multiselect('rebuild');

        //         $("#checkGenres").val(movieGenres);
        //         $('#checkGenres').multiselect('rebuild');

        //         //$("#moviePoster").val(movie.poster);
        //         $('.custom-file-label').html(movie.poster);
        //         $("#checkProducers").val(movie.producer.id).change();
        //         $("#moviePlot").val(movie.plot);

        //     },
        //     error: function(error){
        //         console.log(error)
        //     }
        // })

        var moviePoster = $($(this.parentNode).find("img")).attr("src");
        var movieName = $($(this.parentNode).find(".movieName")).text();
        var movieActors = $($(this.parentNode).find(".movieActors")).text().split(",");
        var movieGenres = $($(this.parentNode).find(".movieGenres")).text().split(",");
        var movieProducer = $($(this.parentNode).find(".movieProducer")).text();
        console.log(moviePoster,movieName,movieActors,movieGenres,movieProducer);
        // console.log(myArr);

    
    });
    

    $('#editMovieButton').click(function(e){
        console.log(this);
        location.href = 'index.html';
        var movieName = $("#movieName").val();
        var movieYOR = $("#movieYOR").val();
        var movieProducer = $("#checkProducers").val();
        var movieActors = $("#checkActors").val();
        var movieGenres = $("#checkGenres").val();
        var moviePoster = $("#moviePoster").val();
        var moviePlot = $("#moviePlot").val();

        //let moviePoster = new FormData($("#moviePoster")[0]);

        var poster = $('.custom-file-label').html();
        
        var formData = new FormData();
        formData.set('file',$('input[type=file]')[0].files[0])
        //console.log(formData);
        //if(formData.length)
        
        //validations
        var valid = true;
        if(movieName==''){
            $("#movieName").addClass("is-invalid");
            valid = false;
        }else{
            $("#movieName").removeClass("is-invalid");
        }
        if(movieYOR==''){
            $("#movieYOR").addClass("is-invalid");
            valid = false;
        }else{
            $("#movieYOR").removeClass("is-invalid");
        }
        if(movieProducer==''){
            $("#movieProducer").addClass("is-invalid");
            valid = false;
        }else{
            $("#movieProducer").removeClass("is-invalid");
        }
        if(movieActors.length==0){
            $("#checkActorsFeedback").css("display", "block");
            valid = false;
        }else{
            $("#checkActorsFeedback").css("display", "none");
        }
        if(movieGenres.length==0){
            $("#checkGenresFeedback").css("display", "block");
            valid = false;
        }else{
            $("#checkGenresFeedback").css("display", "none");
        }
        if(moviePoster===undefined){
            $("#moviePoster").addClass("is-invalid");
            valid = false;
        }else{
            $("#moviePoster").removeClass("is-invalid");
        }
        if(moviePlot==''){
            $("#moviePlot").addClass("is-invalid");
            valid = false;
        }else{
            $("#moviePlot").removeClass("is-invalid");
        }

        var movieId = localStorage.getItem('movieId');
        
        if(valid){
            if(moviePoster != ''){
                $.ajax({ 
                    url:"https://localhost:5001/api/movies/upload",
                    type: "POST",
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function(resultPoster){
                        var data = {
                            "name": movieName,
                            "year": movieYOR,
                            "plot": moviePlot,
                            "actors": movieActors,
                            "genres": movieGenres,
                            "producerId": movieProducer,
                            "poster": resultPoster
                        }
                
                        $.ajax({ 
                            url:`https://localhost:5001/api/movies/${movieId}`,
                            datatype: "json",
                            type: "PUT",
                            data: JSON.stringify(data),
                            contentType: 'application/json',
                            success: function(result){
                                $("#movieName").val('');
                                $("#movieYOR").val('');
                                $("#movieProducer").val('');
                                $("#checkActors").val('');
                                $('#checkActors').multiselect('rebuild');
                                $("#checkGenres").val('');
                                $('#checkGenres').multiselect('rebuild');
                                $("#moviePoster").val('');
                                $("#moviePlot").val('');
                                $('.custom-file-label').html("choose file");
                                $('#editMovieModal').modal('toggle');
                                alertbox.show('Movie Edited successfully');
                                location.reload()
                            },
                            error: function(error){
                                console.log(error)
                            }
                        })
                    },
                    error: function(error){
                        console.log(error)
                    }
                })
            }else{
                var data = {
                    "name": movieName,
                    "year": movieYOR,
                    "plot": moviePlot,
                    "actors": movieActors,
                    "genres": movieGenres,
                    "producerId": movieProducer,
                    "poster": poster
                }
        
                $.ajax({ 
                    url:`https://localhost:5001/api/movies/${movieId}`,
                    datatype: "json",
                    type: "PUT",
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    success: function(result){
                        $("#movieName").val('');
                        $("#movieYOR").val('');
                        $("#movieProducer").val('');
                        $("#checkActors").val('');
                        $('#checkActors').multiselect('rebuild');
                        $("#checkGenres").val('');
                        $('#checkGenres').multiselect('rebuild');
                        $("#moviePoster").val('');
                        $("#moviePlot").val('');
                        $('.custom-file-label').html("choose file");
                        $('#editMovieModal').modal('toggle');
                        alertbox.show('Movie Edited successfully');
                        location.reload()
                    },
                    error: function(error){
                        console.log(error)
                    }
                })
            }
        }else{
            alertbox.show('Error editing movie');
        }
    });


    //alert box
    var AlertBox = function(id, option) {
        this.show = function(msg) {
          if (msg === ''  || typeof msg === 'undefined' || msg === null) {
            throw '"msg parameter is empty"';
          }
          else {
            var alertArea = document.querySelector(id);
            var alertBox = document.createElement('DIV');
            var alertContent = document.createElement('DIV');
            var alertClose = document.createElement('A');
            var alertClass = this;
            alertContent.classList.add('alert-content');
            alertContent.innerText = msg;
            alertClose.classList.add('alert-close');
            alertClose.setAttribute('href', '#');
            alertBox.classList.add('alert-box');
            alertBox.appendChild(alertContent);
            if (!option.hideCloseButton || typeof option.hideCloseButton === 'undefined') {
              alertBox.appendChild(alertClose);
            }
            alertArea.appendChild(alertBox);
            alertClose.addEventListener('click', function(event) {
              event.preventDefault();
              alertClass.hide(alertBox);
            });
            if (!option.persistent) {
              var alertTimeout = setTimeout(function() {
                alertClass.hide(alertBox);
                clearTimeout(alertTimeout);
              }, option.closeTime);
            }
          }
        };
      
        this.hide = function(alertBox) {
          alertBox.classList.add('hide');
          var disperseTimeout = setTimeout(function() {
            alertBox.parentNode.removeChild(alertBox);
            clearTimeout(disperseTimeout);
          }, 500);
        };
      };
      
      var alertbox = new AlertBox('#alert-area', {
        closeTime: 5000,
        persistent: false,
        hideCloseButton: false
      });
});