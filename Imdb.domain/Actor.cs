﻿using System;

namespace Imdb.domain
{
    public class Actor
    {
        public string Name { get; set; }

        public DateTime DateOfBirth { get; set; }

        public Actor(string name, DateTime dob)
        {
            Name = name;
            DateOfBirth = dob;
        }

        public Actor()
        {

        }
    }
}