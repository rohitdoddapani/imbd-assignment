﻿using System;

namespace Imdb.domain
{
    public class Producer
    {
        public string Name { get; set; }

        public DateTime DateOfBirth { get; set; }

        public Producer(string name, DateTime dob)
        {
            Name = name;
            DateOfBirth = dob;
        }

        public Producer()
        {
        }
    }
}