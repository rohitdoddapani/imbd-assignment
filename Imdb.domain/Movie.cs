﻿using System;
using System.Collections.Generic;

namespace Imdb.domain
{
    public class Movie
    {
        public string Name { get; set; }

        public int Year { get; set; }

        public string Plot { get; set; }

        public List<Actor> Actors { get; set; }

        public Producer Producer { get; set; }

        public Movie(string name, int year, string plot,List<Actor> actors,Producer producer)
        {
            Name = name;
            Year = year;
            Plot = plot;
            Actors = actors;
            Producer = producer;
        }

        public Movie()
        {
        }
    }
}